# -*- coding: utf-8 -*-
# LibTracker main library with core tracking functions

import ua
import datetime
from antigate import AntiGate

class LibTracker(object):
	def __init__(self, cur, cfg, intnl=False):
		self.cur = cur
		self.limit = int(cfg.get('tracking', 'limit'))
		self.hours = int(cfg.get('tracking', 'hours'))
		self.config = cfg
		self.internal = intnl

	def workflow(self):
		if self.internal:
			ukr_internal = self.__get_internal()
			if len(ukr_internal) > 0:
				self.__main_track(ukr_internal, True)
		else:
			exported = self.__get_exported()
			self.limit -= len(exported)
			not_exported = self.__get_not_exported()
			if len(exported) > 0:
				self.__main_track(exported, True)
			if len(not_exported) > 0:
				self.__main_track(not_exported)

	@staticmethod
	def __compose_select_qr(table, fields=None, conds=None, order=None, limit=None):
		qrlst = ['select {0} from {1}'.format(','.join(fields) if fields else '*', table)]
		if conds: qrlst.append('where ' + ' and '.join(conds))
		if order: qrlst.append('order by ' + order)
		if limit: qrlst.append('limit {0}'.format(limit))
		return ' '.join(qrlst)

	def __get_tracks(self, **params):
		self.cur.execute(self.__compose_select_qr(**params))
		return self.cur.fetchall()

	def __get_not_exported(self):
		return self.__get_tracks(
			table='tracks',
			conds=(
				'(TimeStampDiff(Hour, dateLastCheck, Now()) >= {0} or TimeStampDiff(Hour, dateLastCheck, now()) is null)'.format(self.hours),
				'dateExport is null',
				'dateDeliver is null',
			),
			order='dateCreate desc, source',
			limit=self.limit
		)

	def __get_exported(self):
		return self.__get_tracks(
			table='tracks',
			conds=(
				'(TimeStampDiff(Hour, dateLastCheck, Now()) >= {0} or TimeStampDiff(Hour, dateLastCheck, now()) is null)'.format(self.hours),
				'dateExport is not null',
				'dateDeliver is null',
			),
			order='-dateCreate',
			limit=self.limit
		)

	def __get_internal(self):
		return self.__get_tracks(
			table='internal_tracks',
			conds=(
				'(TimeStampDiff(Hour, dateLastCheck, Now()) >= {0} or TimeStampDiff(Hour, dateLastCheck, now()) is null)'.format(self.hours),
				'dateDeliver is null'
			),
			order='-dateCreate',
			limit=self.limit
		)

	def __main_track(self, tracks, only_internal=False):
		for row in tracks:
			to_update = {}
			srcpost_instance = None
			if not only_internal:
				src = row['number'][-2:].lower()
				self.cur.execute(self.__compose_select_qr(table='services', conds=('ccode = %s', 'trackingEnabled = 1')), src)
				if self.cur.rowcount > 0:
					mod = __import__(src)
					post_class = getattr(mod, src.capitalize() + 'Post')
					srcpost_instance = post_class(row['number'], self)
					srcpost_instance.customData = row['customData']
					try:
						result = srcpost_instance.execute()
					except Exception:
						print row['id'], row['number']
						raise
					if result:
						for dtn, date in srcpost_instance.dates.iteritems():
							if date != row[dtn]:
								to_update[dtn] = date.strftime('%Y-%m-%d %H:%M:%S') if isinstance(date, datetime.datetime) else date
			uapost_instance = ua.UaPost(row['number'], self)
			try:
				result = uapost_instance.execute()
			except Exception:
				print row['id'], row['number']
				raise
			if result:
				new_dates = set(uapost_instance.dates)-set(srcpost_instance.dates) if srcpost_instance else uapost_instance.dates.keys()
				for dtn in new_dates:
					if row[dtn] != uapost_instance.dates[dtn]:
						to_update[dtn] = uapost_instance.dates[dtn].strftime('%Y-%m-%d %H:%M:%S')
			self.__update_track_dates(to_update, row['id']) if len(to_update) > 0 else self.__update_last_check(row['id'])

	def __update_track_dates(self, dates, tid):
		tbl = 'tracks' if not self.internal else 'internal_tracks'
		qr = 'update ' + tbl + ' set {}, dateLastCheck=now() where id=%s'.format(', '.join('{}=%s'.format(k) for k in dates))
		values = dates.values()
		values.append(tid)
		self.cur.execute(qr, values)

	def __update_last_check(self, tid):
		tbl = 'tracks' if not self.internal else 'internal_tracks'
		qr = 'update ' + tbl + ' set dateLastCheck=now() where id=%s'
		self.cur.execute(qr, tid)

	def antigate_ocr(self, captcha, config=None):
		grab_conf = {'timeout': 30}
		return str(AntiGate(self.config.get('antigate', 'api_key'), captcha, send_config=config, grab_config=grab_conf))
