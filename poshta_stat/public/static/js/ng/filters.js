angular.module('psApp')
	.filter('ifEmpty', function ifEmpty() {
		return function(input, defaultValue) {
			if (angular.isUndefined(input) || input === null || input === '') {
				return defaultValue;
			}

			return input;
		};
	})
	.filter('ucfirst', function ucfirst() {
		return function(input) {
			return !angular.isUndefined(input) ? input.charAt(0).toUpperCase() + input.slice(1) : input;
		}
	});
