# -*- coding: utf-8 -*-
# AzerPost tracking plugin

from base import Base
from dateutil.parser import parse
from tidylib import tidy_document

class AzPost(Base):
	def __init__(self, track, instance):
		super(AzPost, self).__init__(track, instance)
		self.trackurl = 'http://www.azems.az/track_and_trace.php'
		self.need_dop = False
		self.datesDesc = {
			'dateAccept': ('Posting/collection', 'The item is pre-advised'),
			'dateExport': 'Departure from outward office of exchange',
		}

	def execute(self):
		html = self.getUrl(self.trackurl, True, {'item1': self.tracknum, 'br_kind': '750', 'lang': '1'})
		if not html or 'is not in database' in html:
			return False
		html = tidy_document(html, options={'show-body-only': 'y', 'drop-font-tags': 'y'})[0]
		ops = self.xpath('//table[1]/tr[position()>3]/td[position()<4]/text()', html)
		if not ops:
			self.parse_err = 1
			return False
		ops = self.arChunk(map(lambda x: x.strip(), ops), 3)
		for op in ops:
			for name, desc in self.datesDesc.iteritems():
				if (isinstance(desc, str) and desc in op[2]) or (isinstance(desc, tuple) and any(p in op[2] for p in desc)):
					self.dates[name] = parse('{} {}'.format(op[0], op[1].replace('.', ':')), dayfirst=True)
		return len(self.dates) > 0
