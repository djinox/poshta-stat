# -*- coding: utf-8 -*-
# Taiwan Post tracking plugin (all kinds of parcels)
# 0.1 - first version
# 0.2 - added captcha support

from base import Base
from dateutil.parser import parse
import json
import re
import os
import binascii

class TwPost(Base):
	def __init__(self, track, instance):
		super(TwPost, self).__init__(track, instance)
		self.trackurl = "http://postserv.post.gov.tw/pstmail/EsoafDispatcher"
		self.need_dop = False
		self.captcha = '/tmp/twpost-%s.jpg' % binascii.b2a_hex(os.urandom(4))
		self.jsr_pattern = (
			'{"header":{"InputVOClass":"com.systex.jbranch.app.server.post.vo.EB500201InputVO",'
			'"TxnCode":"EB500201","BizCode":"query","StampTime":true,"SupvPwd":"","TXN_DATA":{},'
			'"SupvID":"","CustID":"","REQUEST_ID":"","ClientTransaction":true,"DevMode":false,'
			'"SectionID":"esoaf"},"body":{"MAILNO":"%s","uuid":"77862432-fb3d-46a9-9ebb-7136f67c6280",'
			'"captcha":"%s","pageCount":10}}'
		)
		self.datesDesc = {
			'dateAccept': 'Acceptance',
			'dateExport': 'Departure from outgoing office'
		}

	@staticmethod
	def __dateconvert(date):
		dt = '%s %s %s %s:%s' % (date[0:3], date[3:5], date[5:9], date[9:11], date[11:13])
		return dt

	def execute(self):
		self.getUrl('http://postserv.post.gov.tw/pstmail/SessionServlet')
		self.getUrl('http://postserv.post.gov.tw/pstmail/jcaptcha?uuid=77862432-fb3d-46a9-9ebb-7136f67c6280', fname=self.captcha)
		captcha = self.lib.antigate_ocr(self.captcha, {'numeric': '1'})
		os.remove(self.captcha)
		self.hdrs['Content-Type'] = 'application/json;charset=UTF-8'
		js = self.getUrl(self.trackurl, True, self.jsr_pattern % (self.tracknum, captcha))
		if not js:
			return False
		parsed = json.loads(js)
		try:
			for name, desc in self.datesDesc.iteritems():
				for event in parsed[0]['body']['host_rs']['ITEM']:
					if desc in event['STATUS']:
						self.dates[name] = parse(self.__dateconvert(event['DATIME']))
		except (KeyError, IndexError):
			return False
		return len(self.dates) > 0
