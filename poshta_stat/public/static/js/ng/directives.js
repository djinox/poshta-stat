// paginator directive
angular.module('psApp')
	.directive('paginator', ['$window', function($window) {
		return {
			restrict: 'E',
			scope: {
				totalPages: '=',
				currentPage: '=',
				changeCb: '='
			},
			link: function(scope, element) {
				scope.$watch('totalPages', function(oldVal, newVal) {
					if (oldVal != newVal) {
						element.hide();
						if (scope.totalPages > 1) {
							element.find('#paginator').bootstrapPaginator({
								currentPage: scope.currentPage,
								totalPages: scope.totalPages,
								size: 'normal',
								alignment: 'center',
								onPageClicked: scope.changeCb,
								useBootstrapTooltip: true,
								numberOfPages: 10,
								tooltipTitles: function (type, page) {
									switch (type) {
										case 'first':
											return $window.gettext("Первая страница");
										case "prev":
											return $window.gettext("Предыдущая страница");
										case "next":
											return $window.gettext("Следующая страница");
										case "last":
											return $window.gettext("Последняя страница");
										case "page":
											return $window.gettext("Страница") + ' ' + page;
										default:
											return type;
									}
								}
							});
							element.show();
						}
					}
				}, true);
			}
		}
	}])
	// datepicker directive
	.directive('datepicker', ['$timeout', function($timeout) {
		return {
			restrict: 'E',
			scope: {
				changeCb: '='
			},
			link: function(scope, element, attrs) {
				return $timeout(function() {
					var dp = element.find('#datepicker');
					dp.datepicker({
						startDate: attrs.minDate,
						endDate: attrs.maxDate,
						maxViewMode: 1,
						todayBtn: "linked",
						language: attrs.langCode,
						autoclose: true,
						disableTouchKeyboard: true,
						format: 'dd.mm.yyyy',
						weekStart: 1
					}).on('changeDate', scope.changeCb);
				});
			}
		};
	}])
	// add form trigger button
	.directive('addFormTrigger', function() {
		return {
			restrict: 'A',
			link: function(scope, element) {
				element.bind('click', function() {
					$('#add-form-block').reveal({
						animation: 'fadeAndPop',
						animationspeed: 300,
						closeonbackgroundclick: true,
						dismissmodalclass: 'close-reveal-modal'
					});
				});
			}
		};
	})
	.directive('bindEnter', function() {
		return {
			restrict: 'A',
			link: function(scope, element, attrs) {
				element.bind('keydown keypress keyup', function(event) {
					if (event.which === 13) {
						scope.$eval(attrs.bindEnter);
					}
				});
			}
		};
	})
	.directive('strToNum', function() {
		return {
			require: 'ngModel',
			restrict: 'A',
			link: function(scope, element, attrs, ngModel) {
				ngModel.$parsers.push(function(value) {
					return '' + value;
				});
				ngModel.$formatters.push(function(value) {
					return parseInt(value);
				});
			}
		};
	})
;
