# -*- coding: utf-8 -*-
# Malaysia post tracking plugin

from base import Base
import json
from dateutil.parser import parse

class MyPost(Base):
	def __init__(self, track, instance):
		super(MyPost, self).__init__(track, instance)
		self.trackurl = 'http://api.pos.com.my/v2TrackNTraceWebApi/api/Details/%s?Culture=en&callback=_&sKey=7oHHMvxM0'
		self.need_dop = False
		self.timeout = 10
		self.datesDesc = {
			'dateAccept': 'Item posted',
			'dateExport': 'Item dispatched out KIEV PI-1',
		}

	def execute(self):
		js = self.getUrl(self.trackurl % self.tracknum)
		if not js:
			return False
		try:
			js = json.loads(js[2:-2])
		except ValueError:
			self.parse_err = 1
			return False
		if not isinstance(js, list):
			self.parse_err = 2
			return False
		js.reverse()
		for name, desc in self.datesDesc.iteritems():
			for oper in js:
				if desc in oper['process']:
					self.dates[name] = parse(oper['date'])
					break
		return len(self.dates) > 0
