# -*- coding: utf-8 -*-
# USPS tracking plugin

from base import Base
from dateutil.parser import parse
import xml.etree.ElementTree as ET

class UsPost(Base):
	def __init__(self, track, instance):
		super(UsPost, self).__init__(track, instance)
		self.trackurl = 'http://production.shippingapis.com/ShippingAPI.dll?API=TrackV2&XML=%s'
		self.userid = '870PRIVA6454'
		self.xml_pattern = '<TrackFieldRequest USERID="%s"><Revision>1</Revision><ClientIp>127.0.0.1</ClientIp><SourceId>John</SourceId><TrackID ID="%s"></TrackID></TrackFieldRequest>'
		self.need_dop = False
		self.datesDesc = {
			'dateAccept': ['Acceptance', 'Shipment Accepted', 'Accepted at USPS Origin Sort Facility', 'Electronic Shipping Info Received', 'Arrived at Sort Facility'],
			'dateExport': ['Processed Through Sort Facility', 'Processed Through Facility'],
		}

	def execute(self):
		xml = self.getUrl(self.trackurl % self.xml_pattern % (self.userid, self.tracknum))
		if not xml:
			return False
		xml = ET.fromstring(xml)
		if xml.find('Error'):
			return False
		dates, events = [], []
		for item in xml.iter('TrackSummary'):
			if not item.find('EventCountry').text and item.find('EventDate').text:
				dates.append('%s %s' % (item.find('EventDate').text, item.find('EventTime').text))
				events.append('%s, %s' % (item.find('Event').text, item.find('EventCity').text))
		for item in xml.iter('TrackDetail'):
			if not item.find('EventCountry').text and item.find('EventDate').text:
				dates.append('%s %s' % (item.find('EventDate').text, item.find('EventTime').text))
				events.append('%s, %s' % (item.find('Event').text, item.find('EventCity').text))
		dates.reverse()
		events.reverse()
		for name, desc in self.datesDesc.iteritems():
			for i, event in enumerate(events):
				if any(pattern in event for pattern in desc):
					if name == 'dateExport':
						if 'ISC' in event and 'USPS' in event:
							self.dates[name] = parse(dates[i])
							break
					self.dates[name] = parse(dates[i])
					break
		return len(self.dates) > 0