# -*- coding: utf-8 -*-
# UkrPoshta tracking plugin

from base import Base
from dateutil.parser import parse
from hashlib import sha256
from pytz import timezone
from urlparse import urlparse
import re
import datetime
import xmltodict

class UaPost(Base):
	def __init__(self, track, instance):
		super(UaPost, self).__init__(track, instance)
		self.trackurl = 'http://services.ukrposhta.ua/bardcodesingle/'
		self.need_dop = False
		self.datesDesc = {
			'dateAccept': [u'Приймання'],
			'dateExport': [u'Відправлення міжнародного відправлення з вихідної установи обміну'],
			'dateImport': [u'Надходження міжнародного відправлення в установу обміну країни призначення', u'Надходження до установи обміну України'],
			'dateImportLeave': [u'Відправка міжнародного відправлення з установи обміну країни призначення', u'Відправка з установи обміну до об\'єкту поштового зв\'язку України'],
			'dateEnterCustoms': [u'Проходить митне оформлення', u'Знаходиться в митниці', u'Передача на митний контроль', u'Передано для митного контролю*', u'Передано для митного контролю'],
			'dateArrive': [u'Надходження до відділення зв’язку, що доставляє', u'Надходження до відділення зв’язку', u'Відправлення до ВПЗ'],
			'dateDeliver': [u'Вручення', u'Вручення адресату особисто', u'Вручення члену сім\'ї', u'Вручення за довіреністю'],
		}
		self.dates_desc_api = {
			'dateAccept': {10101},
			'dateExport': {70801},
			'dateImport': {60701},
			'dateEnterCustoms': {80821, 80819},
			'dateImportLeave': {90801},
			'dateArrive': {21501, 20901, 31410, 31419, 51120},
			'dateDeliver': {41002, 41003, 41004, 41022}
		}
		self.datesDescEms = {
			'dateImport': ['Arrival at inward office of exchange'],
			'dateImportLeave': ['Departure from inward office of exchange'],
			'dateEnterCustoms': ['Held by Customs'],
			'dateArrive': ['Arrival at Delivery office'],
			'dateDeliver': ['Final delivery'],
		}
		self.timeout = 15
		self.trackurl_api = 'http://services.ukrposhta.ua/barcodestatistic/barcodestatistic.asmx/GetBarcodeInfo?guid={}&culture=uk&barcode={}'
		self.api_guid = 'fcc8d9e1-b6f9-438f-9ac8-b67ab44391dd'

	@staticmethod
	def __get_ua_datetime():
		return datetime.datetime.now(timezone('Europe/Kiev'))

	def track_via_api(self):
		xml = self.getUrl(self.trackurl_api.format(self.api_guid, self.tracknum))
		if not xml:
			self.parse_err = 15
			return False

		try:
			xml = xmltodict.parse(xml)
			ecode = int(xml['BarcodeInfoService']['code'])
		except TypeError:
			return False
		except (Exception, KeyError):
			self.parse_err = 16
			return False

		for dtn, desc in self.dates_desc_api.iteritems():
			if ecode in desc:
				self.dates[dtn] = parse(xml['BarcodeInfoService']['eventdate'], dayfirst=True)
				break
		return len(self.dates) > 0

	# DEPRECATED NOW but maybe will work sometime in future again
	def track_via_site(self):
		html = self.getUrl(self.trackurl)
		vwstate = False
		if html:
			vwstate = self.xpath('//input[@id="__VIEWSTATE"]/@value', html)
			if vwstate:
				vwstate = vwstate[0]

		if not vwstate:
			self.parse_err = 1
			return False

		post_data = {
			'__ASYNCPOST': 'true',
			'__VIEWSTATE': vwstate,
			'__VIEWSTATEGENERATOR': 'F0813681',
			'ctl00$centerContent$txtBarcode': self.tracknum,
			'ctl00$centerContent$btnFindBarcodeInfo': u'Пошук',
			'ctl00$centerContent$scriptManager': 'ctl00$centerContent$scriptManager|ctl00$centerContent$btnFindBarcodeInfo'
		}
		html = self.getUrl(self.trackurl + 'Default.aspx', True, post_data)
		if not html:
			return False

		match = re.search('pageRedirect\|\|(.+)\|', html)
		if not match:
			self.parse_err = 4
			return False

		pu = urlparse(self.trackurl)
		html = self.getUrl('{}://{}{}'.format(pu.scheme, pu.hostname, match.group(1)))
		if not html:
			return False
		table = self.xpath(
			'//div[@id="ctl00_centerContent_divPerformedOperations"]/*/table/tr[position()>2]/td[@style="padding:5px;text-align:left"]/text()',
			html)
		if not table:
			self.parse_err = 3
			return False
		table = map(lambda s: s.strip(), table)
		table = self.arChunk(table, 5)
		table.reverse()
		for dtName, desc in self.datesDesc.iteritems():
			for status in table:
				if status[4] in desc:
					self.dates[dtName] = parse(status[0], dayfirst=True)
					break
		return len(self.dates) > 0

	def execute(self):
		if re.match('(R|C|L|\d+)', self.tracknum):
			return self.track_via_api()
		elif self.tracknum[0] == 'E':
			self.trackurl = 'http://dpsz.ua/en/track/ems'
			post_data = {
				'uds_ems_tr': self.tracknum,
				'track': sha256(self.__get_ua_datetime().strftime('%d.%m.%Y')).hexdigest()
			}
			html = self.getUrl(self.trackurl, True, post_data)
			if not html or 'No data found for' in html:
				return False
			ops = self.xpath('//div[@id="content-col"]/table/tr[count(td)=3]/td[position()<3]', html)
			if not ops:
				self.parse_err = 1
				return False
			ops = map(lambda e: e.text.strip().replace(u'\xa0', ' '), ops)
			ops = self.arChunk(ops, 2)
			for name, desc in self.datesDescEms.iteritems():
				for oper in ops:
					for pattern in desc:
						if pattern == oper[1]:
							self.dates[name] = parse(oper[0], dayfirst=True)
			return len(self.dates) > 0
		return False