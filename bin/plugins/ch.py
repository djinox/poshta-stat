# -*- coding: utf-8 -*-
# Swiss Post tracking plugin

from base import Base
from dateutil.parser import parse

class ChPost(Base):
	def __init__(self, track, instance):
		super(ChPost, self).__init__(track, instance)
		self.trackurl = 'https://www.post.ch/EasyTrack/submitParcelData.do?p_language=en&VTI-GROUP=1&directSearch=false&formattedParcelCodes=%s'
		self.need_dop = False
		self.datesDesc = {
			'dateAccept': ['Mailed'],
			'dateExport': ['Departed transit country'],
		}

	def execute(self):
		html = self.getUrl(self.trackurl % self.tracknum)
		if not html:
			return False
		table = self.xpath('//table[@class="events_view fullview_tabledata"]/tbody/tr/td/text()', html)
		if not table:
			self.parse_err = 1
			return False
		table = map(lambda s: s.strip(), table)
		ops = self.arChunk(table, 6)
		for name,desc in self.datesDesc.iteritems():
			for oper in ops:
				if oper[3] in desc:
					self.dates[name] = parse(' '.join([oper[0], oper[1]]), dayfirst=True)
		return len(self.dates) > 0
