# -*- coding: utf-8 -*-
# ChinaPost tracking plugin

from base import Base
from dateutil.parser import parse
from xml.parsers.expat import ExpatError
import os
import binascii
import subprocess
import xmltodict
import redis
import json
import random

class CnPost(Base):
	def __init__(self, track, instance):
		super(CnPost, self).__init__(track, instance)
		self.trackurl = 'http://intmail.183.com.cn/zdxt/gjyjqcgzcx/gjyjqcgzcx_gjyjqcgzcxLzxxQueryPage.action'
		self.ems_trackurl = 'http://ems.com.cn/ems/order/singleQuery_t'
		self.xml_pattern = '<?xml version="1.0" encoding="utf-8"?><root><params><param><key>vYjhm</key><value>{0}</value></param><param><key>FROM_FLAG</key><value>0</value></param><param><key>gngjFlag</key><value>1</value></param><param><key>validres</key><value>success</value></param></params><data></data></root>'
		self.need_dop = False
		self.timeout = 30
		self.datesDesc = {
			'dateAccept': (u'已收寄',),
			'dateExport': (u'已交航空公司运输', (u'离开', u'交航'), u'已出口直封', u'启运')
		}
		self.datesDescEms = {
			'dateAccept': (u'已收件',),
			'dateExport': (u'离开', u'发往')
		}
		self.datesDescEmsEn = {
			'dateAccept': (u'Posting/Collection',),
			'dateExport': (u'Departure from outward office of exchange',)
		}
		self.captcha = '/tmp/cnpost-%s.jpg' % binascii.b2a_hex(os.urandom(4))
		self.tries = 0
		self.max_tries = 5
		self.proxies_key = 'us-proxies'

	def __ems_captcha_guess(self):
		self.tries += 1
		self.getUrl('http://ems.com.cn/mailtracking/you_jian_cha_xun.html')
		for i in range(self.max_tries):
			captcha_result = self.getUrl(url='http://ems.com.cn/ems/rand', fname=self.captcha)
			if captcha_result and self.captcha_is_image:
				break
		if not self.captcha_is_image:
			if os.path.exists(self.captcha):
				os.remove(self.captcha)
			return False
		#check_code = self.lib.antigate_ocr(self.captcha, {'min_len': '6', 'max_len': '6', 'numeric': '1'})
		check_code = subprocess.check_output(('env', 'tesseract', '-psm', '7', self.captcha, 'stdout')).rstrip()
		os.remove(self.captcha)
		if not check_code:
			return self.__ems_captcha_guess() if self.tries < self.max_tries else False
		html = self.getUrl(self.ems_trackurl, True, {'mailNum': self.tracknum, 'checkCode': check_code})
		if not html:
			return self.__ems_captcha_guess() if self.tries < self.max_tries else False
		return html if self.xpath('//table[@id="showTable"][tr[@id="0"]]', html) else False

	def track_ems(self):
		html = self.__ems_captcha_guess()
		if not html:
			return False
		table = self.xpath('//table[@id="showTable"][tr[@id="0"]]/tr/td[position()<4]', html)
		if not table:
			self.parse_err = 2
			return False
		table = self.arChunk(table, 3)
		text_tbl = []
		for event in table:
			if event[0].text:
				evd = self.__strclean(event[0].text)
				last_date = evd
			else:
				evd = last_date
			text_tbl.append((evd, self.__strclean(event[1].text), self.__strclean(event[2].text)))
		text_tbl.reverse()
		for dtName, desc in self.datesDescEms.iteritems():
			for event in text_tbl:
				if all(x in event[2] for x in desc):
					self.dates[dtName] = parse(' '.join((event[0], event[1])))
					break
		return len(self.dates) > 0

	def track_post(self):
		self.tries += 1
		xml = self.getUrl(self.trackurl, True, {'submitType': '1', 'ajax': self.xml_pattern.format(self.tracknum)})
		if not xml:
			return self.track_post() if self.tries < self.max_tries else False
		try:
			parsed = xmltodict.parse(xml)
		except ExpatError:
			return False
		if not 'rdata' in parsed['root']['data'] or not len(parsed['root']['data']['rdata']):
			return False
		events = list(parsed['root']['data']['rdata'])
		events.reverse()
		for dtName, desc in self.datesDesc.iteritems():
			for event in parsed['root']['data']['rdata']:
				if 'V_ZT' in event and 'D_SJSJ' in event and event['V_GJDM'] == u'CN':
					for part in desc:
						if (isinstance(part, tuple) and all(x in event['V_ZT'] for x in part)) or \
								(isinstance(part, unicode) and part in event['V_ZT']):
							self.dates[dtName] = parse(event['D_SJSJ'])
							break
		return len(self.dates) > 0

	def track_17track(self):
		self.trackurl = 'http://www.17track.net/restapi/handlertrack.ashx'
		rds = redis.StrictRedis()
		proxies = rds.get(self.proxies_key)
		if not proxies:
			return self.track_post()
		random_proxy = random.choice(json.loads(proxies))
		self.set_referer('http://www.17track.net/en/track?nums={0}'.format(self.tracknum))
		js = self.getUrl(self.trackurl, True, '{"guid":"","data":[{"num":"%s"}]}' % self.tracknum, proxy=random_proxy)
		if not js:
			self.parse_err = 13
			return False
		try:
			js = json.loads(js)
		except ValueError:
			self.parse_err = 10
			return False
		try:
			while js['dat'][0]['delay'] != 0:
				js = self.getUrl(self.trackurl, True, '{"guid":"%s","data":[{"num":"%s"}]}' % (js['g'], self.tracknum), proxy=random_proxy)
				if not js:
					return False
				try:
					js = json.loads(js)
				except ValueError:
					self.parse_err = 11
					return False
		except (IndexError, KeyError):
			return False
		is_ems = self.tracknum[0] in ('E', 'L')
		try:
			events = js['dat'][0]['track']['z2'] if is_ems else js['dat'][0]['track']['z1']
		except (KeyError, IndexError):
			self.parse_err = 12
			return False
		if not events:
			return False
		events.reverse()
		dates_desc = self.datesDescEmsEn if is_ems else self.datesDesc
		for dtName, desc in dates_desc.iteritems():
			for event in events:
				for part in desc:
					if (isinstance(part, list) and all(x in event['z'] for x in part)) or \
							(isinstance(part, unicode) and part in event['z']):
						self.dates[dtName] = parse(event['a'])
						break
		return len(self.dates) > 0

	@staticmethod
	def __strclean(s):
		return s.strip().replace(u'\xa0', ' ')

	def execute(self):
		return self.track_ems() if self.tracknum[0] in ('E', 'L') else self.track_post()
