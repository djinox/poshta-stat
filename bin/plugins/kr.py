# -*- coding: utf-8 -*-
# Korea POST tracking plugin

from base import Base
from dateutil.parser import parse
import re

class KrPost(Base):
	def __init__(self, track, instance):
		super(KrPost, self).__init__(track, instance)
		self.trackurl = 'http://trace.epost.go.kr/xtts/servlet/kpl.tts.common.svl.SttSVL'
		self.need_dop = False
		self.datesDesc = {
			'dateAccept': ['Posting/Collection'],
			'dateExport': ['Departure from outward office of exchange'],
		}

	def execute(self):
		postData = {
			'POST_CODE': self.tracknum,
			'ems_gubun': self.tracknum[0],
			'target_command': 'kpl.tts.tt.epost.cmd.RetrieveEmsTraceEngCmd'
		}
		html = self.getUrl(self.trackurl, True, postData)
		if not html or '<div class="result-notFound">' in html:
			return False
		table = self.xpath('//table/tbody/tr/td[not(@class)]/text() | //table/tbody/tr/td[@class]/p[1]', html)
		if not table:
			self.parse_err = 1
			return False
		table = map(self.__filter_str, table)
		table = self.arChunk(table, 4)
		for name, desc in self.datesDesc.iteritems():
			for oper in table:
				if 'Dispatch number' in oper[3]:
					m = re.search(r'\d+', oper[3])
					if m:
						self.dates['bagNo'] = m.group()
				if oper[1] in desc:
					self.dates[name] = parse(oper[0])
					break
		return len(self.dates) > 0

	@staticmethod
	def __filter_str(s):
		if isinstance(s, str) or isinstance(s, unicode):
			return re.sub(r'\s{2,}', ' ', s.strip())
		else:
			return re.sub(r'\s{2,}', ' ', s.text.strip()) if s.text else ''
