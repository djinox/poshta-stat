��             +         �  L   �       �   6     �     �  �   �  h   �  �   3  "   �  J   �  J   :  p   �  ^   �     U     c  ?   �  Q   �  M        a  5   t  #   �  @   �  N   	  %   ^	  #   �	     �	     �	  C   �	     
     
     9
  �  ?
     �  	     [        y  	   �  X   �  4   �  G        `  .   o  )   �  =   �  %        ,  
   3     >     X  (   r     �      �  	   �  :   �  #        )  	   7     A     I     N     n     t     �     
                                                    	                                                                                       Введите текст с проверочного изображения Вручено адресату Данный тип трека пока что не поддерживается системой, либо трек введен неверно. Добавить Добавлено новых Дополнительные данные трека введены в неподдерживаемом формате.<br>Введите данные согласно всплывающей подсказке. Контрольная сумма трека не совпадает, проверьте трек-код Контрольная сумма трека неверна - пожалуйста, проверьте правильность ввода номера. Обнаружена ошибка! Отправка из места международного обмена Отправка из страны отправителя (экспорт) Ошибка добавления трека, попробуйте повторить попытку позже. Ошибка запроса, попробуйте повторить попытку позже Ошибка! Первая страница Передача на таможенное оформление Перейти к статистике по данному направлению По указанным критериям посылок не найдено Подсказка Подтвердите, что Вы - не робот Последняя страница Поступление в ММПО Украины (импорт) Поступление в почтовое отделение вручения Предыдущая страница Следующая страница Сообщение Страница Страница отслеживания данного трека Трек добавлен успешно н/д Project-Id-Version: PACKAGE VERSION
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2014-03-28 14:47+0200
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
Language: en
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Enter the text from captcha Delivered This tracking number type is not supported by the system yet or tracking number is invalid. Submit Submitted Additional track data format is not supported.<br>Enter the data according to a tooltip. Tracking number checksum is invalid, check it please Tracking number checksum is invalid - please check the number validity. Error occured! Dispatched from international exchange office  Dispatched from sender's country (export) Error occured while track submitting, please try again later. Request error, please try again later Error! First page Sent to customs clearance Go to stats on this route No parcels found on your search criteria Tip Confirm, that you're not a robot Last page Income to Ukrainian international exchange office (import) Arrived at post office for delivery Previous page Next page Message Page Go to this tracking number page Track submitted successfully n/a 