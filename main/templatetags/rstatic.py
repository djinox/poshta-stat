from django import template
from django.conf import settings
from django.core.cache import cache
import hashlib

register = template.Library()

@register.simple_tag
def dist_static(path):
	key = 'rev:' + hashlib.md5(path).hexdigest()
	rev = cache.get(key)
	return '{0}{1}{2}'.format(settings.PROD_DIST_URL, path, '?' + rev if rev else '')

@register.simple_tag
def cstatic(path):
	return ''.join((settings.PROD_STATIC_URL if not settings.DEBUG else settings.STATIC_URL, path))