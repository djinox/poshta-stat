# -*- coding: utf-8 -*-
import re
import requests
import datetime
from selenium.webdriver import Remote
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from pytz import timezone
from urllib import unquote
from django.core.cache import cache
from hashlib import sha1

def validate_track_checksum(num):
	res = int(num[2])*8 + int(num[3])*6 + int(num[4])*4 + int(num[5])*2 
	res += int(num[6])*3 + int(num[7])*5 + int(num[8])*9 + int(num[9])*7
	cnum = res % 11
	if cnum == 0:
		return True if num[10] == '5' else False
	elif cnum == 1:
		return True if num[10] == '0' else False
	else:
		return True if int(num[10]) == 11-cnum else False

def validate_track_regex(num):
	return re.search(r'^(\d{13}|[RCEL][A-Z]\d{9}[A-Z]{2})$', num)

def recaptcha_check(code, ip, privatekey):
	params = {'response': code, 'remoteip': ip, 'secret': privatekey}
	r = requests.post('https://www.google.com/recaptcha/api/siteverify', data=params)
	return r.json()['success']

def get_sorting(request, internal):
	sort_dates = ['id', 'export', 'importd', 'importleave', 'arrive', 'deliver']
	sort_dates_int = ['id', 'accept', 'arrive', 'deliver']
	sort_arr = sort_dates if not internal else sort_dates_int
	sess_key = 'sort' if not internal else 'sort_int'
	if request.POST.get('sort', False):
		try:
			request.session[sess_key] = sort_arr[int(request.POST['sort'])]
			return request.session[sess_key]
		except IndexError:
			return 'id'
	if request.session.get(sess_key, False):
		return request.session[sess_key]
	return 'id'

def dateformat(dt):
	return dt.strftime('%d.%m.%Y') if dt else None

def timeformat(dt):
	return dt.strftime('%H:%M') if dt else None

def get_ua_datetime():
	return datetime.datetime.now(timezone('Europe/Kiev'))

def spider_check(request):
	spiders = ['Slurp', 'Yandex', 'bingbot']
	agent = request.META.get('HTTP_USER_AGENT', None)
	return any(s in agent for s in spiders)

def spider_handler(request):
	if spider_check(request):
		full_url = '{scheme}://{host}{path}'.format(scheme=request.scheme, host=request.get_host(), path=request.path)
		qs = request.META['QUERY_STRING']
		if qs.startswith('_escaped_fragment_='):
			full_url += unquote(qs.replace('_escaped_fragment_=', '#!'))
		cache_hash = sha1(full_url).hexdigest()
		html = cache.get(cache_hash)
		if not html:
			try:
				wd = Remote(
					command_executor='http://127.0.0.1:8910/wd/hub',
					desired_capabilities=DesiredCapabilities.PHANTOMJS
				)
				wd.get(full_url)
				html = wd.page_source
				cache.set(cache_hash, html, 172800)
				wd.close()
				wd.quit()
			except Exception:
				return False
		return html
	else:
		return False

def is_crawler(request):
	ua = request.META.get('HTTP_USER_AGENT', None)
	return 'PhantomJS' in ua if ua else False