#-*- coding: utf-8 -*-
# Sweden post tracking plugin (directlink singapore support)

from base import Base
from dateutil.parser import parse
import json

class SePost(Base):
	def __init__(self, track, instance):
		super(SePost, self).__init__(track, instance)
		self.trackurl = 'http://directlinktrackedplus.com/responseStatus.php?json=1&lang=en&postal_ref_no=%s'
		self.need_dop = False
		self.timeout = 20
		self.datesDesc = {
			'dateAccept': ['1', '16', '18'],
			'dateExport': ['11', '2'],
		}
		
	def execute(self):
		# direct link singapore track
		if self.tracknum[0:2] in ('RE', 'RD'):
			js = self.getUrl(self.trackurl % self.tracknum)
			if not js:
				return False
			
			try:
				js = json.loads(js)
			except ValueError:
				return False

			if 'item_events' not in js or len(js['item_events']) == 0:
				return False
			#js['item_events'].reverse()
			for name, value in self.datesDesc.iteritems():
				found = False
				for val in js['item_events']:
					if val[1] in value:
						try:
							self.dates[name] = parse(val[0]).replace(tzinfo=None)
						except ValueError:
							continue
						found = True
						break
				if found:
					continue
			return len(self.dates) > 0
		else:
			return False