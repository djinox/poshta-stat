# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from django.conf import settings
from django.views.decorators.csrf import ensure_csrf_cookie
from django.core.exceptions import ObjectDoesNotExist
from django.http import HttpResponse
from django.utils.translation import ugettext as _, check_for_language, LANGUAGE_SESSION_KEY
from django.utils import translation
from django.views.decorators.cache import cache_page
from django.views.i18n import javascript_catalog
from urlparse import urlparse
import templatetags.customfilters as cf
import datetime
import models
import utils
import json

# Create your views here.
def init_view(request):
	vals = dict()
	vals['enabled_srv_normal'] = models.Services.objects.get_enabled()
	vals['popular_srv'] = models.Services.objects.get_popular()
	vals['recaptcha_public'] = settings.RECAPTCHA_PUBLIC
	vals['is_crawler'] = utils.is_crawler(request)
	vals['DEBUG'] = settings.DEBUG
	addform_js_vars = {'trackRegexs': [], 'extraRegexs': {}, 'extraTrackRegexs': {}, 'extraTooltips': {}}
	for item in vals['enabled_srv_normal']:
		addform_js_vars['trackRegexs'].append(item.regex)
		if item.needextra:
			addform_js_vars['extraRegexs'][item.ccode] = item.extraregex
			if item.extratrackregex:
				addform_js_vars['extraTrackRegexs'][item.ccode] = item.extratrackregex
			if item.extratooltip:
				addform_js_vars['extraTooltips'][item.ccode] = item.extratooltip
	vals['addform_js_vars'] = json.dumps(addform_js_vars)
	return vals

def __plainresp(html):
	return HttpResponse(html, content_type='text/html; charset=utf8')

# Index page
@ensure_csrf_cookie
def index(request):
	response = utils.spider_handler(request)
	if response:
		return __plainresp(response)
	vals = init_view(request)
	vals['enabled_srv'] = map(None, *([iter(models.Services.objects.get_enabled())] * 2))
	today = datetime.date.today()
	yesterday = today - datetime.timedelta(days=1)
	vals['td_stat'] = models.Tracks.objects.get_count_statistics(today)
	vals['yd_stat'] = models.Tracks.objects.get_count_statistics(yesterday)
	vals['int_td_stat'] = models.InternalTracks.objects.get_count_statistics(today)
	vals['int_yd_stat'] = models.InternalTracks.objects.get_count_statistics(yesterday)
	vals['page'] = 'index'
	return render(request, 'index.html', vals)

# Main stat page
@ensure_csrf_cookie
def stat(request):
	response = utils.spider_handler(request)
	if response:
		return __plainresp(response)
	vals = init_view(request)
	vals['page'] = 'stat'
	vals['showaddbutton'] = True
	vals['internal'] = False
	vals['keywords'] = u', %s' % _(u'общая статистика')
	vals['description'] = _(u'Общая статистика по всем странам с возможностью выбора страны и типа посылки.')
	return render(request, 'stat.html', vals)

# Global summary page
@ensure_csrf_cookie
def summary(request):
	response = utils.spider_handler(request)
	if response:
		return __plainresp(response)
	vals = init_view(request)
	vals['showaddbutton'] = True
	vals['page'] = 'summary'
	vals['keywords'] = u', %s' % _(u'сводная статистика')
	vals['description'] = _(u'Сводная статистика по всем направлениям.')
	today = utils.get_ua_datetime()
	vals['max_date'] = today.strftime('%d.%m.%Y')
	vals['min_date'] = (today - datetime.timedelta(days=60)).strftime('%d.%m.%Y')
	vals['country_map'] = {}
	for item in vals['enabled_srv_normal']:
		vals['country_map'][item.ccode] = _(item.country)
	vals['country_map'] = json.dumps(vals['country_map'])
	return render(request, 'summary.html', vals)

# project about page
@ensure_csrf_cookie
def about(request):
	response = utils.spider_handler(request)
	if response:
		return __plainresp(response)
	vals = init_view(request)
	vals['showaddbutton'] = True
	vals['page'] = 'about'
	vals['keywords'] = u', %s' % _(u'о проекте')
	vals['description'] = _(u'Информация о проекте Пошта-стат.')
	return render(request, 'about.html', vals)

# detailed track stats page
@ensure_csrf_cookie
def track(request, tid):
	response = utils.spider_handler(request)
	if response:
		return __plainresp(response)
	vals = init_view(request)
	if not tid.isdigit():
		return redirect('/')
	try:
		trackobj = models.Tracks.objects.get(id=tid)
	except ObjectDoesNotExist:
		return redirect('/')
	types = ['R', 'C', 'E', 'L']
	vals['parcelType'] = types.index(trackobj.number[0])+1
	vals['showaddbutton'] = True
	vals['track'] = trackobj
	vals['keywords'] = u', %s, %s' % (_(trackobj.source.country), trackobj.source.postname)
	vals['description'] = _(u'Подробная статистика по треку %(track)s (%(postname)s, %(country)s).') % {
		'track': cf.trackhide(trackobj.number),
		'postname': trackobj.source.postname,
		'country': _(trackobj.source.country)
	}
	return render(request, 'track.html', vals)

# disqus comments page
@ensure_csrf_cookie
def feedback(request):
	response = utils.spider_handler(request)
	if response:
		return __plainresp(response)
	vals = init_view(request)
	vals['showaddbutton'] = True
	vals['page'] = 'feedback'
	vals['keywords'] = u', %s' % _(u'отзывы')
	vals['description'] = _(u'Отзывы и предложения для проекта Пошта-стат.')
	return render(request, 'feedback.html', vals)

# set language redirect view
def setlang(request, lang_code):
	if lang_code and check_for_language(lang_code):
		translation.activate(lang_code)
		request.session[LANGUAGE_SESSION_KEY] = lang_code
	if request.META.get('HTTP_REFERER', None):
		parsed = urlparse(request.META['HTTP_REFERER'])
		url = request.META['HTTP_REFERER'] if parsed.netloc == request.META['HTTP_HOST'] else '/'
	else:
		url = '/'
	return redirect(url)

# cached js translations catalog
@cache_page(43200)
def cached_js_catalog(request, domain='djangojs', packages=None):
	return javascript_catalog(request, domain, packages)