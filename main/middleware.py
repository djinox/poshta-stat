import json
from django.utils.translation import LANGUAGE_SESSION_KEY
from django.contrib.gis.geoip import GeoIP
from ipware.ip import get_ip

class ForceDefaultLanguageMiddleware(object):
	"""
	Ignore Accept-Language HTTP headers
	
	This will force the I18N machinery to always choose settings.LANGUAGE_CODE
	as the default initial language, unless another one is set via sessions or cookies
	
	Should be installed *before* any middleware that checks request.META['HTTP_ACCEPT_LANGUAGE'],
	namely django.middleware.locale.LocaleMiddleware
	"""
	def process_request(self, request):
		if request.META.has_key('HTTP_ACCEPT_LANGUAGE'):
			del request.META['HTTP_ACCEPT_LANGUAGE']
		if LANGUAGE_SESSION_KEY not in request.session and 'setlang' not in request.path:
			ccode = GeoIP().country_code(get_ip(request))
			if ccode == 'RU':
				request.session[LANGUAGE_SESSION_KEY] = 'ru'
			elif ccode == 'UA':
				request.session[LANGUAGE_SESSION_KEY] = 'uk'

class ExtractParamsFromJSONPostRequestMiddleware(object):
	def process_request(self, request):
		if request.META['CONTENT_TYPE'].startswith('application/json'):
			try:
				js = json.loads(request.body)
				if len(js) > 0:
					request.POST = request.POST.copy()
					for param, value in js.iteritems():
						request.POST[param] = value
			except ValueError:
				pass
