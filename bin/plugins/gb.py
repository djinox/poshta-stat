# -*- coding: utf-8 -*-
# Royal Mail / ParcelForce (UK) tracking plugin

from base import Base
from dateutil.parser import parse

class GbPost(Base):
	def __init__(self, track, instance):
		super(GbPost, self).__init__(track, instance)
		self.trackurl = 'https://www.royalmail.com/track-your-item'
		self.need_dop = False
		self.datesDesc = {
			'dateAccept': ['accepted', 'collected', 'sent at post office'],
			'dateExport': ['despatched to', 'on its way tokiev  comm. of ind. states'],
		}
		self.datesDescEms = {
			'dateAccept': ['Collected from customer'],
			'dateExport': ['Forwarded for export', 'Prepared for export by air'],
		}

	def execute(self):
		return self.track_ems() if self.tracknum[0] in ('E', 'C') else self.track_post()

	def track_post(self):
		pdata = {
			'tracking_number': self.tracknum,
			'op': 'Track item',
			'form_build_id': 'form-a8632a2b1cc05941bf2b4e531543e161',
			'form_id': 'rml_track_trace_search_form'
		}
		html = self.getUrl(self.trackurl, True, pdata)
		if not html or 'there has been a problem' in html:
			return False
		am = self.xpath('//tr[@class="even" or @class="odd"]/td/text()', html)
		if not am:
			self.parse_err = 1
			return False
		ops = self.arChunk(am, 4)
		ops.reverse()
		for name, desc in self.datesDesc.iteritems():
			found = False
			for op in ops:
				if op[1] != 'AM' and op[1] != 'PM':
					dt = '%s %s' % (op[0], op[1])
				else:
					dt = op[0]
				for pattern in desc:
					if pattern in op[2].lower():
						self.dates[name] = parse(dt, dayfirst=True)
						found = True
						break
				if found:
					break
		return len(self.dates) > 0

	def track_ems(self):
		self.trackurl = 'http://www.parcelforce.com/track-trace'
		pdata = {
			'parcel_tracking_number': self.tracknum,
			'op': 'Track item',
			'form_build_id': 'form-eCxt1EJZeBk29HHDGiYaQpSYjaAz5VLl3UEtyd1HBR8',
			'form_id': 'track_and_trace_form'
		}
		html = self.getUrl(self.trackurl, True, pdata)
		ops = self.xpath('//td[@class="tracking-history-date" or @class="tracking-history-time" or @class="tracking-history-desc"]/text()', html)
		if not ops:
			self.parse_err = 2
			return False
		ops = self.arChunk(ops, 3)
		for name, desc in self.datesDescEms.iteritems():
			for oper in ops:
				dt = ' '.join([oper[0], oper[1]])
				if oper[2] in desc:
					self.dates[name] = parse(dt, dayfirst=True)
					break
		return len(self.dates) > 0