angular.module('psApp').controller(
	'addFormCtrl',
	['$scope', '$window', '$timeout', '$http', '$compile', 'messi',
	function addFormCtrl($scope, $window, $timeout, $http, $compile, messi) {
		var entryPoint = '/api/add_track';

		for (var key in $window.PS.afo) {
			if ($window.PS.afo.hasOwnProperty(key)) {
				$scope[key] = $window.PS.afo[key];
			}
		}

		$scope.formData = {};
		$scope.formIsValid = false;
		$scope.lastCheckedBC = null;
		$scope.customDataDisabled = true;
		$scope.addButtonDisabled = false;
		$scope._messi = null;

		$scope.checkTrackChecksum = function(num) {
			var res = (num[2] * 8 + num[3] * 6 + num[4] * 4 + num[5] * 2 + num[6] * 3 + num[7] * 5 + num[8] * 9 + num[9] * 7) % 11;
			if (res === 0) {
				return num[10] == '5';
			} else if (res === 1) {
				return num[10] == '0';
			} else {
				return num[10] == 11 - res;
			}
		};

		$scope.closeAll = function() {
			$('#add-form-block').trigger('reveal:close');
			if ($scope._messi) {
				$scope._messi.unload();
			}
		};

		$scope.barcodeCheck = function() {
			return $timeout(function() {
				var bcElem = $('#barcode'),
					barcode = $scope.formData.barcode.toUpperCase(),
					cdElem = $('#customData'),
					destroyPopovers = function() {
						bcElem.popover('destroy');
						cdElem.popover('destroy');
					},
					showPopover = function(el, content, title) {
						el.popover({content: content, trigger: 'manual', title: title});
						el.popover('show');
					},
					etr = $scope.extraTrackRegexs,
					et = $scope.extraTooltips;

				if ($scope.lastCheckedBC != barcode) {
					if (/^[RCE][A-Z]\d{9}[A-Z]{2}$/.test(barcode)) {
						var ccode = barcode.slice(-2).toLowerCase();

						if (!$scope.checkTrackChecksum(barcode)) {
							showPopover(bcElem, gettext('Контрольная сумма трека не совпадает, проверьте трек-код'));
						} else {
							destroyPopovers();
							if (et[ccode] && (!etr[ccode] || (etr[ccode] && new RegExp('^' + etr[ccode] + '$').test(barcode)))) {
								$scope.customDataDisabled = false;
								showPopover(cdElem, et[ccode], gettext('Подсказка'));
							}
						}
					} else {
						$scope.customDataDisabled = true;
						destroyPopovers();
					}

					$scope.lastCheckedBC = barcode;
				}
			}, 100);
		};

		$scope.validateForm = function() {
			var found = false;
			for (var regex in $scope.trackRegexs) {
				if ($scope.trackRegexs.hasOwnProperty(regex)
					&& new RegExp('^' + $scope.trackRegexs[regex] + '$').test($scope.formData.barcode)
				) {
					found = true;
					break;
				}
			}

			if (!found) {
				return gettext('Данный тип трека пока что не поддерживается системой, либо трек введен неверно.');
			}

			if (!/^\d{13}$/.test($scope.formData.barcode) && !$scope.checkTrackChecksum($scope.formData.barcode)) {
				return gettext('Контрольная сумма трека неверна - пожалуйста, проверьте правильность ввода номера.');
			}

			if (!$window.grecaptcha.getResponse()) {
				return gettext('Подтвердите, что Вы - не робот');
			}

			if (!/^\d{13}$/.test($scope.formData.barcode)) {
				var
					ccode = $scope.formData.barcode.slice(-2).toLowerCase(),
					er = $scope.extraRegexs,
					etr = $scope.extraTrackRegexs;

				if (er[ccode] 
					&& !new RegExp('^' + er[ccode] + '$').test($scope.formData.customData)
					&& (!etr[ccode] || (etr[ccode] && new RegExp('^' + etr[ccode] + '$').test($scope.formData.barcode)))
				) {
					return gettext('Дополнительные данные трека введены в неподдерживаемом формате.<br>Введите данные согласно всплывающей подсказке.');
				}
			}

			return true;
		};
		
		$scope.submitAdd = function() {
			var validationResult = $scope.validateForm();

			if (validationResult !== true) {
				new messi(validationResult, {title: gettext('Ошибка!'), titleClass: 'anim error', modal: true});
				$window.grecaptcha.reset();
				return false;
			}

			$scope.addButtonDisabled = true;
			$scope.formData['g-recaptcha-response'] = $window.grecaptcha.getResponse();

			$http.post(entryPoint, $scope.formData).success(function(data) {
				if (data.status) {
					var msgTmpl = '<span>{{ m.track }} <strong>{{ m.barcode }}</strong> {{ m.successadd }}!<br/>'
						+ '<strong><a href="/stat/?service={{ m.service }}" ng-click="closeAll()">{{ m.gotostat }}</a><br/>'
						+ '<a href="/track/{{ m.trackid }}/" ng-click="closeAll()">{{ m.trackpage }}</a></strong></span>';

					$scope._messi = new messi(
						$compile(angular.element(msgTmpl))($scope),
						{title: gettext('Сообщение'), titleClass: 'anim success', modal: true}
					);
					$scope.m = {
						track: gettext('Трек'),
						barcode: $scope.formData.barcode,
						service: data.service,
						successadd: gettext('добавлен успешно'),
						gotostat: gettext('Перейти к статистике по данному направлению'),
						trackid: data.track_id,
						trackpage: gettext('Страница отслеживания данного трека')
					};

					$scope.formData = {barcode: '', customData: ''};
				} else {
					new messi(data.msg, {title: gettext('Обнаружена ошибка!'), titleClass: 'anim error'});
				}
			}).error(function() {
				new messi(
					gettext('Ошибка добавления трека, попробуйте повторить попытку позже.'),
					{title: gettext('Ошибка!'), titleClass: 'anim error'}
				);
			}).finally(function() {
				$scope.addButtonDisabled = false;
				$window.grecaptcha.reset();
			});

			return true;
		};
	}]
);