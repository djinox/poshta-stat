angular.module('psApp').controller(
	'statsCtrl',
	['$scope', '$http', '$location', '$rootScope', '$window', 'QSService', 'DataShare',
	function statsCtrl($scope, $http, $location, $rootScope, $window, QSService, DataShare) {
		var entryPoint = '/api/get_tracks';
		var defaultFilterParams = {
			page: '1',
			sort: '0',
			parcelType: '0',
			service: '0',
			bagNumber: null
		};
		var refFilterParams = JSON.parse(JSON.stringify(defaultFilterParams));
		var sortFields = ['id', 'export', 'importd', 'importleave', 'arrive', 'deliver'];
		var customSearchFields = ['bagNumber'];

		$scope.searchIsActive = false;
		$scope.filterParams = {};

		$scope.validate = function() {
			for (var k in $scope.filterParams) {
				if ($scope.filterParams.hasOwnProperty(k)) {
					if ($scope.filterParams[k] != null && parseInt($scope.filterParams[k]) != $scope.filterParams[k]) return false;
				}
			}
			return true;
		};

		$scope.doSearch = function() {
			$scope.showTable = false;
			$scope.showError = false;
			$scope.searchIsActive = true;
			$scope.errorText = null;
			$scope.data = {};
			$scope.showCustomFilters = false;

			$http.post(entryPoint, $scope.filterParams).success(function(data) {
				if (data.status) {
					$scope.data = data;
					$rootScope.pageTitle = 'Пошта-Стат - ' + $scope.data.tableTitle;
					if (data.tracks.length > 0) {
						$scope.showTable = true;
						$scope.initSort();
					} else {
						$scope.errorText = 'По указанным критериям посылок не найдено';
					}
				} else {
					$scope.errorText = 'Ошибка запроса, попробуйте повторить попытку позже';
				}

				$scope.showError = $scope.errorText !== null;
			}).error(function() {
				$scope.errorText = 'Ошибка запроса, попробуйте повторить попытку позже';
				$scope.showError = true;
			}).finally(function() {
				$scope.searchIsActive = false;
			});
		};

		$scope.qsUpdate = function() {
			$location.search($scope.filterParams);
		};

		$scope.setPage = function(event, originalEvent, type, page) {
			$scope.filterParams.page = page;
			$scope.qsUpdate();
			$scope.$digest();
			$window.scrollTo(0, 0);
		};

		$scope.setSort = function(sort) {
			$scope.filterParams.sort = sort.toString();
			$scope.filterParams.page = '1';
			$scope.qsUpdate();
		};

		$scope.resetFilters = function() {
			$scope.filterParams.page = '1';
			$scope.filterParams.bagNumber = null;
		};

		$scope.initSort = function() {
			var id = sortFields.indexOf($scope.data.sort);
			if (id < 0) id = 0;
			id = id.toString();
			if ($scope.filterParams.sort != id) {
				$scope.filterParams.sort = id;
			}

			$('i[class=icon-chevron-down]').removeClass('icon-chevron-down');
			$('#i_' + $scope.data.sort).addClass('icon-chevron-down');
		};

		$scope.filterParams = QSService.getFilters(defaultFilterParams);
		if (!$scope.validate()) {
			$scope.filterParams = refFilterParams;
			$location.search({});
		}

		$scope.$watch(function() { return $location.search(); }, function() {
			QSService.getFilters($scope.filterParams);
			DataShare.service = $scope.filterParams.service;
			$scope.doSearch();
		}, true);

		$scope.$watch(function() { return $scope.data; }, function() {
			for (var i = 0; i < customSearchFields.length; i++) {
				if ($scope.data[customSearchFields[i]]) {
					$scope.showCustomFilters = true;
					break;
				}
			}
		}, true);

		// init custom filter fields
		for (var i=0; i < customSearchFields.length; i++) {
			switch (customSearchFields[i]) {
				case 'bagNumber':
					$('#bagNumber').typeahead({
						source: function(query, process) {
							$http.post('/api/lookup_bag', {service: $scope.filterParams.service, query: query}).success(function(data) {
								if (data.status) {
									return process(data.bagNumbers);
								}
							});
						},
						items: 10,
						minLength: 2
					});
			}
		}
	}]
);
