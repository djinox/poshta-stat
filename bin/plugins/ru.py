# -*- coding: utf-8 -*-
# Russian Post tracking plugin

import json
from base import Base
from dateutil.parser import parse

class RuPost(Base):
	def __init__(self, track, instance):
		super(RuPost, self).__init__(track, instance)
		self.trackurl = 'https://www.pochta.ru/tracking?p_p_id=trackingPortlet_WAR_portalportlet&p_p_lifecycle=2&p_p_state=normal&p_p_mode=view&p_p_resource_id=getList&p_p_cacheability=cacheLevelPage&p_p_col_id=column-1&p_p_col_pos=1&p_p_col_count=2&barcodeList={}&postmanAllowed=true'
		self.need_dop = False
		self.timeout = 10
		self.datesDesc = {
			'dateAccept': (1, 1),
			'dateExport': (10, 0)
		}

	def execute(self):
		js = self.getUrl(self.trackurl.format(self.tracknum))
		if not js:
			return False
		try:
			js = json.loads(js)
		except ValueError:
			self.parse_err = 1
			return False
		try:
			history = js['list'][0]['trackingItem']['trackingHistoryItemList']
		except (KeyError, TypeError):
			self.parse_err = 2
			return False
		if not history:
			return False
		history = [e for e in history if e['countryName'] == u'Russia']
		history.reverse()
		for name, desc in self.datesDesc.iteritems():
			for event in history:
				if (event['operationType'], event['operationAttr']) == desc:
					self.dates[name] = parse(event['date']).replace(tzinfo=None, microsecond=0)
					break
		return len(self.dates) > 0
