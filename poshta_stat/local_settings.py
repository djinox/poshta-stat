# Local settings for poshta_stat project.
LOCAL_SETTINGS = True
from settings import *
import socket
import re

DEBUG = bool(not re.match(r'\w+\.trustno\.one$', socket.getfqdn()))

# Make this unique, and don't share it with anybody.
SECRET_KEY = ')1u0l+x2p#07cda%=lc9(#dkq2&n+hvv8a(c)x$e&&oiaxjaf0'
RECAPTCHA_PRIVATE = '6Le2mRMTAAAAAO2Ea8njauYG5MTUjGV4m2zGoIk8'
RECAPTCHA_PUBLIC = '6Le2mRMTAAAAAJ-UuTWqhILtzi-jzWYsd58rBruo'

CACHES = {
	'default': {
		'BACKEND': 'django.core.cache.backends.locmem.LocMemCache',
		'LOCATION': 'poshta-stat'
	}
}

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.mysql',
		'NAME': 'poshtastat',
		'USER': 'poshtastat',
		'PASSWORD': 'testpassword'
	}
}
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'

if not DEBUG: # prod configuration
	DATABASES['default']['PASSWORD'] = 'Muzon10530'
	DATABASES['default']['PORT'] = 39395
	DATABASES['default']['CONN_MAX_AGE'] = None

	CACHES['default'] = {
		'BACKEND': 'redis_cache.RedisCache',
		'LOCATION': '127.0.0.1:6379',
		'KEY_PREFIX': 'poshta-stat',
		'OPTIONS': {
			'DB': 1,
			'CONNECTION_POOL_CLASS': 'redis.BlockingConnectionPool',
			'CONNECTION_POOL_CLASS_KWARGS': {
				'max_connections': 30,
				'timeout': 60
			}
		}
	}

	EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
	EMAIL_HOST = 'smtp.mailgun.org'
	EMAIL_PORT = 587
	EMAIL_HOST_USER = 'postmaster@inox.mailgun.org'
	EMAIL_HOST_PASSWORD = '2457h99az038'
	EMAIL_USE_TLS = True
