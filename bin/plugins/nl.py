# -*- coding: utf-8 -*-
# NL post tracking plugin

from base import Base
from dateutil.parser import parse

class NlPost(Base):
	def __init__(self, track, instance):
		super(NlPost, self).__init__(track, instance)
		self.trackurl = 'http://postnl.post/details/'
		self.referer = 'http://postnl.post/'
		self.need_dop = False
		self.timeout = 20
		self.datesDesc = {
			'dateAccept': 'item is pre-advised',
			'dateExport': 'item is on transport to the country of destination'
		}

	def execute(self):
		html = self.getUrl(self.trackurl, True, {'barcodes': self.tracknum})
		if not html:
			return False
		am = self.xpath('//tr[contains(@class, "detail")]/td[not(@rowspan)]/text()', html)
		if not am:
			self.parse_err = 1
			return False
		am = map(lambda s: s.strip(), am)
		ops = self.arChunk(am, 2)
		for name, desc in self.datesDesc.iteritems():
			for oper in ops:
				if desc in oper[1]:
					self.dates[name] = parse(oper[0], dayfirst=True)
					break
		return len(self.dates) > 0
