# -*- coding: utf-8 -*-

from django.db import models
from django.db.models import Q, Case, When, Sum, IntegerField
from django.core.cache import cache

class BaseTracksManager(models.Manager):
	def get_count_statistics(self, date, source=None, use_cache=True):
		cache_key = '%s_%s' % (self.cache_prefix, date.strftime('%Y%m%d')) if not source \
			else '%s_%s_%s' % (self.cache_prefix, source.ccode, date.strftime('%Y%m%d'))
		cached = cache.get(cache_key)
		if cached and use_cache:
			return cached
		queries = [Q(**{field: date}) for field in self.fields]
		query = queries.pop()
		for item in queries:
			query |= item
		params = [query]
		aggr_args = {}
		for field in self.fields:
			aggr_args[field] = Sum(Case(When(**{field: date, 'then': 1}), output_field=IntegerField()))
		if source:
			params.append(Q(source=source))
		result = self.filter(*params).aggregate(**aggr_args)
		for k, v in result.items():
			if v is None:
				result[k] = 0
		cache.set(cache_key, result, 900)
		return result

	def get_detailed_stats(self, date):
		counts = self.get_count_statistics(date=date, use_cache=False)
		result = {}
		for field in self.fields:
			result[field] = list(self.filter(**{field: date}))
		return [result, counts]

class TracksManager(BaseTracksManager):
	fields = ['create', 'accept', 'export', 'importd', 'entercustoms', 'importleave', 'arrive', 'deliver']
	cache_prefix = 'stat'

class InternalTracksManager(BaseTracksManager):
	fields = ['create', 'accept', 'arrive', 'deliver']
	cache_prefix = 'int_stat'

class BaseTracksWithDatesManager(models.Manager):
	def get_queryset(self):
		if 'create' in self.fields:
			self.fields.remove('create')
		queries = [Q(**{'%s__isnull' % field: False}) for field in self.fields]
		query = queries.pop()
		for item in queries:
			query |= item
		return super(BaseTracksWithDatesManager, self).get_queryset().filter(query)

class TracksWithDatesManager(BaseTracksWithDatesManager):
	fields = list(TracksManager.fields)

class InternalTracksWithDatesManager(BaseTracksWithDatesManager):
	fields = list(InternalTracksManager.fields)

class ServicesManager(models.Manager):
	def get_enabled(self):
		cached = cache.get('enabled_srv')
		if cached:
			return cached
		result = self.filter(enabled=True).order_by('country')
		cache.set('enabled_srv', result, 1800)
		return result

	def get_popular(self):
		cached = cache.get('popular_srv')
		if cached:
			return cached
		result = self.filter(popular=True)
		cache.set('popular_srv', result, 1800)
		return result
