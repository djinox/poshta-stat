# -*- coding: utf-8 -*-
import json
import utils
import math
import models
import re
import datetime
import templatetags.customfilters as cf
from django.conf import settings
from django.http import HttpResponse
from django.core.exceptions import ObjectDoesNotExist
from ipware.ip import get_ip
from dateutil.relativedelta import relativedelta
from pytz import timezone
from django.utils.translation import ugettext as _

def __jsonresp(arr):
	return HttpResponse(json.dumps(arr), content_type='application/json')

def execute(request, method):
	if request.method != 'POST':
		return __jsonresp({'status': False, 'msg': 'Request not allowed'})
	try:
		return __jsonresp(globals()[method + '_action'](request))
	except KeyError:
		return __jsonresp({'status': False, 'msg': 'Method does not exist'})

# api query for track add
def add_track_action(request):
	req_params = ['barcode', 'g-recaptcha-response']
	for param in req_params:
		if param not in request.POST:
			return {'status': False, 'msg': 'Not enough params for this method!'}
	barcode = request.POST['barcode']
	if not utils.validate_track_regex(barcode):
		return {'status': False, 'msg': 'Wrong barcode format!'}
	if not utils.recaptcha_check(request.POST['g-recaptcha-response'], get_ip(request), settings.RECAPTCHA_PRIVATE):
		return {'status': False, 'msg': _(u'Код с изображения введен неверно, повторите ввод')}
	internal = True if re.search(r'^\d{13}$', barcode) else False
	trackobj = getattr(models, 'Tracks') if not internal else getattr(models, 'InternalTracks')
	if not internal:
		if not utils.validate_track_checksum(barcode):
			return {'status': False, 'msg': _(u'Неправильная контрольная сумма у трек-номера')}
		found = False
		services = models.Services.objects.get_enabled()
		for service in services:
			if re.search(r'^%s$' % service.regex, barcode):
				found = True
				srvid = service.id
				break
		if not found:
			return {'status': False, 'msg': _(u'Треки данного вида пока что не поддерживаются системой')}
	else:
		srvid = 1
	res = trackobj.objects.filter(number=barcode, create__gte=datetime.date.today()-relativedelta(months=6)).values_list('id', flat=True)
	if res:
		return {'status': False, 'msg': _(u'Данный трек уже есть в системе, добавлять его не нужно.<br>Вы можете <a href=\'/track/%(id)s/\'><strong>перейти на страницу с треком</strong></a>.') % {'id': res[0]}}
	res = trackobj.objects.create(number=barcode, source=service, customdata=request.POST.get('customData', None)) if not internal else trackobj.objects.create(number=barcode)
	return {'status': True, 'msg': 'OK', 'service': srvid, 'track_id': res.id} if res else {'status': False, 'msg': _(u'Не удалось сохранить трек, пожалуйста повторите операцию позже')}

# query for get country stats
def get_stats_action(request):
	service = request.POST.get('service', False)
	srv = False
	values = dict()
	values['status'] = True
	internal = service == '1'
	if service and service != '0':
		try:
			srv = models.Services.objects.get(id=service, enabled=True)
			values['image'] = srv.ccode
		except ObjectDoesNotExist:
			return {'status': False, 'msg': 'Wrong service id'}
	else:
		values['image'] = 'int_24'
	today = utils.get_ua_datetime()
	yesterday = today - datetime.timedelta(days=1)
	trackobj = getattr(models, 'Tracks') if not internal else getattr(models, 'InternalTracks')
	values['tdStat'] = trackobj.objects.get_count_statistics(today, srv)
	values['ydStat'] = trackobj.objects.get_count_statistics(yesterday, srv)
	return values

# query to get detailed stats
def get_stats_detailed_action(request):
	date = request.POST.get('date', False)
	today = utils.get_ua_datetime()
	try:
		parsed_date = timezone('Europe/Kiev').localize(datetime.datetime.strptime(date, '%d.%m.%Y'))
	except ValueError:
		return {'status': False, 'msg': 'wrong date specified'}
	if today < parsed_date or (today - parsed_date).days > 60:
		return {'status': False, 'msg': 'date is not in permitted range'}
	stats = models.Tracks.objects.get_detailed_stats(parsed_date)
	grp_stats = dict()
	for datetype, tracks in stats[0].iteritems():
		for track in tracks:
			grp_stats.setdefault(datetype, {}) \
				.setdefault(track.number[-2:], {}) \
				.setdefault(track.number[0], []) \
				.append({'id': track.id, 'time': utils.timeformat(getattr(track, 'date' + datetype))})
	values = dict()
	values['status'] = True
	values['detailedStats'] = grp_stats
	values['detailedCount'] = stats[1]
	values['day'] = date
	return values

# api query for tracks fetch
def get_tracks_action(request):
	per_page = 50
	filters = {}
	values = {'status': True, 'filters': {'parcelType': 0, 'service': 0}}
	service = request.POST.get('service', False)
	cur_page = request.POST.get('page', False)
	parcel_type = request.POST.get('parcelType', False)
	bag_number = request.POST.get('bagNumber', False)
	parcel_types = ['R', 'C', 'E', 'L']

	if bag_number and bag_number != '0':
		filters['bagno'] = bag_number
		values['filters']['bagNumber'] = bag_number

	if parcel_type and parcel_type != '0':
		try:
			filters['number__startswith'] = parcel_types[int(parcel_type)-1]
			values['filters']['parcelType'] = parcel_type
		except IndexError:
			pass
	if not cur_page:
		cur_page = 1
	internal = service == '1'
	lim_from = (int(cur_page) - 1) * per_page
	lim_to = lim_from + per_page
	values['sort'] = utils.get_sorting(request, internal)
	values['curPage'] = cur_page
	srv = None
	if service and service != '0':
		try:
			srv = models.Services.objects.get(id=service, enabled=True)
		except ObjectDoesNotExist:
			return {'status': False, 'redirect': True, 'path': '/stat/'}
		filters['source'] = srv
		values['filters']['service'] = srv.id
		values['bagNumber'] = srv.hasbagno
	if internal:
		count = models.InternalTracks.withdates.count()
		tracks = models.InternalTracks.withdates.order_by('-%s' % values['sort'])[lim_from:lim_to]
		values['tableTitle'] = _(u'Статистика доставки внутренних посылок по Украине')
	else:
		count = models.Tracks.withdates.filter(**filters).count()
		tracks = models.Tracks.withdates.filter(**filters).order_by('-%s' % values['sort'])[lim_from:lim_to]
		values['tableTitle'] = _(u'cтатистика доставки посылок в Украину ')
		values['tableTitle'] += _(u'из %(where)s (%(pname)s)') % {'where': _(srv.genitive), 'pname': srv.postname} if srv else _(u'по всем зарубежным направлениям')
		
	values['totalPages'] = int(math.ceil(float(count) / per_page))
	values['tracks'] = []
	for track in tracks:
		trackobj = {'daysBetween': [], 'id': track.id, 'code': cf.trackhide(track.number)}
		if not internal:
			trackobj['export'] = utils.dateformat(track.export)
			trackobj['daysBetween'].append(cf.daysdiff(track.export, track.importd))
			trackobj['importd'] = utils.dateformat(track.importd)
			trackobj['daysBetween'].append(cf.daysdiff(track.importd, track.importleave))
			trackobj['importleave'] = utils.dateformat(track.importleave)
			trackobj['daysBetween'].append(cf.daysdiff(track.importleave, track.arrive))
		else:
			trackobj['accept'] = utils.dateformat(track.accept)
			trackobj['daysBetween'].append(cf.daysdiff(track.accept, track.arrive))
		trackobj['arrive'] = utils.dateformat(track.arrive)
		trackobj['daysBetween'].append(cf.daysdiff(track.arrive, track.deliver))
		trackobj['deliver'] = utils.dateformat(track.deliver)
		trackobj['totalDays'] = cf.calcdays(track)
		values['tracks'].append(trackobj)
	return values

# api query for bag number search
def lookup_bag_action(request):
	req_params = ['query', 'service']
	for param in req_params:
		if param not in request.POST:
			return {'status': False, 'msg': 'Not enough params for this method!'}
	query = request.POST['query']
	service = request.POST['service']
	bags = models.Tracks.objects.filter(bagno__startswith=query, source=service) \
		.distinct() \
		.order_by('bagno') \
		.values_list('bagno', flat=True)
	return {'status': True, 'bagNumbers': map(str, list(bags))}
