# -*- coding: utf-8 -*-
# JapanPost tracking plugin

from base import Base
from dateutil.parser import parse

class JpPost(Base):
	def __init__(self, track, instance):
		super(JpPost, self).__init__(track, instance)
		self.trackurl = 'https://trackings.post.japanpost.jp/services/srv/search/direct?searchKind=S004&locale=en&reqCodeNo1=%s'
		self.need_dop = False
		self.datesDesc = {
			'dateAccept': ['Posting/Collection'],
			'dateExport': ['Dispatch from outward office of exchange']
		}

	def execute(self):
		html = self.getUrl(self.trackurl % self.tracknum)
		if not html:
			return False
		ops = self.xpath('//table[@class="tableType01 txt_c m_b5"][2]/tr/td/text()', html)
		if not ops:
			return False
		ops = self.arChunk(ops, 5)
		for name, desc in self.datesDesc.iteritems():
			for oper in ops:
				if oper[1] in desc:
					self.dates[name] = parse(oper[0])
		return len(self.dates) > 0
