# -*- coding: utf-8 -*-
# Belarus Post tracking plugin

from base import Base
from dateutil.parser import parse

class ByPost(Base):
	def __init__(self, track, instance):
		super(ByPost, self).__init__(track, instance)
		self.trackurl = 'http://declaration.belpost.by/searchRu.aspx?search=%s'
		self.need_dop = False
		self.datesDesc = {
			'dateAccept': u'Принято почтовое отправление',
			'dateExport': u'Отправка отправления из учреждения обмена',
		}

	def execute(self):
		html = self.getUrl(self.trackurl % self.tracknum)
		if not html or u'По вашему запросу ничего не найдено' in html:
			return False
		ops = self.xpath('//table/tr/td[not(@class) and not(contains(., "MINSK"))]', html)
		if not ops:
			self.parse_err = 1
			return False
		ops = self.arChunk(ops, 2)
		for op in ops:
			for name, desc in self.datesDesc.iteritems():
				if desc in op[1].text_content():
					self.dates[name] = parse(op[0].text_content(), dayfirst=True)
		return len(self.dates) > 0