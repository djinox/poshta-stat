# -*- coding: utf-8 -*-
# Eesti post -> worktrace.com plugin

from base import Base
from dateutil.parser import parse

class EePost(Base):
	def __init__(self, track, instance):
		super(EePost, self).__init__(track, instance)
		self.trackurl = 'http://80.65.129.230/servlet/tracktrace.tAndTServletLBV1'
		self.trackurl2 = 'http://80.65.129.230/servlet/authentification.LoginServlet161214?user=ESTONIA&language=UK&WTact=public&WTCONTEXT=tandtCritImbedLB'
		self.need_dop = False
		self.datesDesc = {
			'dateExport': ('Included in Dispatch', 'Departure from Outward OE'),
			'dateAccept': ('Posting',),
		}

	def execute(self):
		if not self.getUrl(self.trackurl2):
			return False
		pdata = {
			'ITEMVal': self.tracknum,
			'ITEMCrit': '=',
			'WTact': 'select',
			'MASTERCrit': '=',
			'MASTERVal': 'ESTONIA'
		}
		html = self.getUrl(self.trackurl, True, pdata)
		if not html:
			return False
		elms = self.xpath('//form[@name="entryfields"]/table[3]//tr/td[@class="trace" and (position()=2 or position()=4)]/text()', html)
		if not elms:
			self.parse_err = 1
			return False
		elms = map(lambda x: x.replace(u'\xa0', ''), elms)
		elms = self.arChunk(elms, 2)
		elms.reverse()
		for name, desc in self.datesDesc.iteritems():
			for elem in elms:
				if any(p in elem[0] for p in desc):
					self.dates[name] = parse(elem[1])
					break
		return len(self.dates) > 0
