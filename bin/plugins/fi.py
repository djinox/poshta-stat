# -*- coding: utf-8 -*-
from base import Base
from dateutil.parser import parse
import json

class FiPost(Base):
	def __init__(self, track, instance):
		super(FiPost, self).__init__(track, instance)
		self.trackurl = 'http://www.posti.fi/henkiloasiakkaat/seuranta/api/shipments/{}'
		self.need_dop = True
		self.datesDesc = {
			'dateAccept': (u'Item has arrived to warehouse', u'Item has been registered'),
			'dateExport': (u'The item is on its way to the destination country',)
		}

	def execute(self):
		if self.tracknum[0] == 'R':
			js = self.getUrl(self.trackurl.format(self.tracknum))
			if not js:
				return False
			try:
				js = json.loads(js)
			except ValueError:
				self.parse_err = 1
				return False
			try:
				if len(js['shipments'][0]['events']) > 0:
					for name, desc in self.datesDesc.iteritems():
						for evt in js['shipments'][0]['events']:
							if any(p in evt['description']['en'] for p in desc):
								self.dates[name] = parse(evt['timestamp']).replace(tzinfo=None)
								break
			except (KeyError, IndexError):
				self.parse_err = 2
			return len(self.dates) > 0
		else:
			return False
