from django import template
from django.conf import settings
import datetime

register = template.Library()

@register.filter
def trackhide(num):
	if not isinstance(num, str):
		num = str(num)
	return '%s...%s' % (num[:2], num[-2:])

@register.filter
def trackhide_summary(num):
	if not isinstance(num, str):
		num = str(num)
	return '%s...%s' % (num[0], num[-2:])

@register.filter
def daysdiff(dfrom, dto):
	if not isinstance(dto, datetime.date) or not isinstance(dfrom, datetime.date):
		return '...'
	else:
		return (dto - dfrom).days

@register.filter
def calcdays(track):
	fields = ['accept', 'export', 'importd', 'entercustoms', 'importleave', 'arrive', 'deliver', 'create']
	for field in fields:
		try:
			if getattr(track, field):
				dfrom = getattr(track, field)
				break
		except AttributeError:
			continue
	dto = track.deliver if track.deliver else datetime.date.today()
	return daysdiff(dfrom, dto)

@register.filter
def customdate(date):
	dmy = date.strftime('%d.%m.%Y')
	hm = date.strftime('%H:%M')
	return '%s %s' % (dmy, hm) if hm != '00:00' else dmy

@register.filter
def isocode(code):
	return settings.LANGUAGE_CODE_MAP[code] if code in settings.LANGUAGE_CODE_MAP else code
