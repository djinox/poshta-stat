import sys, ConfigParser
sys.dont_write_bytecode = True

sys.path.append('plugins')
import libtracker
cfg = ConfigParser.ConfigParser()
cfg.read('cfg/tracker.ini')
lib = libtracker.LibTracker(False, cfg)

#num = 'RB006533087SG'
#num = 'RS287422889NL'
#num = 'RI420702539CN'
#num = 'RA823496165TW'
#num = 'RE991734842SE'
#num = 'RC037536450MY'
#num = 'RF066426099CN'
#num = 'RO302678835EE'
#num = 'RR407289279BY'
#num = 'RA165692538FI'
#num = 'RH041890227TR'
num = 'RN005469039AZ'
#src = 'ua'
src = num[-2:].lower()
mod = __import__(src)
postCls = getattr(mod, src.capitalize() + 'Post')
postObj = postCls(num, lib)
#postObj.customData = '15.07.2015'
print postObj.execute()
print postObj.dates
print postObj.parse_err
print postObj.errno
