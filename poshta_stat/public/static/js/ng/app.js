angular.module('psApp', ['ngStorage', 'QuickList'])
	.constant('_', window._)
	.run(['$rootScope', function($rootScope) {
		$rootScope.gettext = window.gettext;
		$rootScope.messi = window.Messi;
		$rootScope.Utils = {
			keys: Object.keys,
			size: _.size
		};
	}])
	.config(['$locationProvider', '$httpProvider', function($locationProvider, $httpProvider) {
		$locationProvider.html5Mode({enabled: true, requireBase: false, rewriteLinks: false});
		$httpProvider.defaults.xsrfCookieName = 'csrftoken';
		$httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
	}]);
