# -*- coding: utf-8 -*-
# Base class tracking plugin

import requests
import lxml.html
import socket
import magic
import os.path
from requests.utils import dict_from_cookiejar
from requests.exceptions import Timeout, ConnectionError, HTTPError, ChunkedEncodingError
from email.utils import formatdate

class Base(object):
	def __init__(self, track, instance=False):
		self.tracknum = track
		self.delivered = False
		self.http_code = 0
		self.errno = 0
		self.timeout = 10
		self.parse_err = 0
		self.http = requests.Session()
		self.curdata = dict()
		self.customData = None
		self.hdrs = dict()
		self.hdrs['User-Agent'] = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/39.0.2171.65 Safari/537.36'
		self.cookies = dict()
		self.dates = dict()
		self.lib = instance
		self.logfile = '/tmp/dates_tracker.log'
		self.debug = False
		self.referer = None

	def set_referer(self, referer):
		self.referer = referer

	def getUrl(self, url, post=False, pdata=None, fname=None, proxy=None):
		if hasattr(self, 'referer'):
			self.hdrs['referer'] = self.referer
		if hasattr(self, 'headers'):
			self.hdrs.update(self.headers)
		try:
			args = {
				'headers': self.hdrs,
				'timeout': self.timeout,
				'allow_redirects': True,
				'cookies': self.cookies
			}
			if proxy:
				args['proxies'] = {'http': 'http://' + proxy, 'https': 'http://' + proxy}
			if not post:
				r = self.http.get(url, **args)
			else:
				args['data'] = pdata
				r = self.http.post(url, **args)
		except (Timeout, socket.timeout):
			self.errno = 1
		except (ConnectionError, socket.error):
			self.errno = 2
		except HTTPError:
			self.errno = 3
		except ChunkedEncodingError:
			self.errno = 4

		if self.errno > 0:
			return False

		if self.debug:
			print r.request.url
			print r.request.body
			print r.request.headers
			print r.headers

		self.http_code = r.status_code
		if fname:
			with open(fname, 'wb') as fp:
				fp.write(r.content)
			return True
		else:
			return r.text

	@staticmethod
	def xpath(query, where):
		tree = lxml.html.fromstring(where)
		return tree.xpath(query)

	@staticmethod
	def arChunk(arr, count):
		return zip(*[iter(arr)] * count)

	def get_cookies(self):
		return dict_from_cookiejar(self.http.cookies)

	def log_event(self, data):
		with open(self.logfile, 'a') as fp:
			logstr = '[%s] %s - %s\n' % (formatdate(usegmt=True), self.__class__.__name__, data.encode('utf-8'))
			fp.write(logstr)

	@property
	def captcha_is_image(self):
		if not os.path.exists(self.captcha):
			return False
		with magic.Magic(flags=magic.MAGIC_MIME_TYPE) as mgc:
			return mgc.id_filename(self.captcha).startswith('image')
