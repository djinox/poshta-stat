from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

admin.autodiscover()

js_info_dict = {
	'packages': ('poshta_stat',),
}

urlpatterns = patterns('main.views',
	url(r'^admin/', include(admin.site.urls)),
	url(r'^$', 'index'),
	url(r'^stat/$', 'stat'),
	url(r'^summary/$', 'summary'),
	url(r'^about/$', 'about'),
	url(r'^track/(\d+)/$', 'track'),
	url(r'^feedback/$', 'feedback'),
	url(r'^setlang/(?P<lang_code>.*)/$', 'setlang'),
	url(r'^jsi18n/$', 'cached_js_catalog', js_info_dict),
)

urlpatterns += patterns('main.api',
	url(r'^api/(\w+)$', 'execute')
)

if settings.DEBUG:
	import debug_toolbar
	urlpatterns += patterns('',
		url(r'^__debug__/', include(debug_toolbar.urls)),
	)