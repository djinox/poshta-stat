from __future__ import unicode_literals

from django.db import models
import managers

class Users(models.Model):
	id = models.AutoField(primary_key=True)
	email = models.CharField(max_length=50)
	password = models.CharField(max_length=64)

	class Meta:
		db_table = 'users'

class Services(models.Model):
	id = models.AutoField(primary_key=True)
	country = models.CharField(max_length=50)
	genitive = models.CharField(max_length=50)
	postname = models.CharField(db_column='postName', max_length=100) 
	ccode = models.CharField(max_length=2)
	customdata = models.TextField(db_column='customData') 
	needextra = models.BooleanField(db_column='needExtra')
	regex = models.CharField(max_length=30)
	extraregex = models.CharField(max_length=50, db_column='extraRegex')
	extratrackregex = models.CharField(max_length=50, db_column='extraTrackRegex')
	extratooltip = models.CharField(max_length=100, db_column='extraTooltip')
	enabled = models.BooleanField()
	popular = models.BooleanField()
	hasbagno = models.BooleanField(db_column='hasBagNo')
	objects = managers.ServicesManager()

	class Meta:
		db_table = 'services'

class BaseTracks(models.Model):
	id = models.AutoField(primary_key=True)
	datecreate = models.DateTimeField(db_column='dateCreate', auto_now_add=True)
	create = models.DateField(auto_now_add=True)
	dateaccept = models.DateTimeField(db_column='dateAccept', blank=True, null=True)
	accept = models.DateField(blank=True, null=True)
	datearrive = models.DateTimeField(db_column='dateArrive', blank=True, null=True)
	arrive = models.DateField(blank=True, null=True)
	datedeliver = models.DateTimeField(db_column='dateDeliver', blank=True, null=True)
	deliver = models.DateField(blank=True, null=True)
	userid = models.IntegerField(db_column='userId', default=0)
	comment = models.CharField(max_length=50, blank=True)
	datelastcheck = models.DateTimeField(db_column='dateLastCheck')

	class Meta:
		abstract = True

class Tracks(BaseTracks):
	number = models.CharField(max_length=13, blank=False, null=False)
	bagno = models.IntegerField(db_column='bagNo', blank=True, null=True)
	source = models.ForeignKey(Services, db_column='source')
	customdata = models.CharField(max_length=50, null=True)
	dateexport = models.DateTimeField(db_column='dateExport', blank=True, null=True)
	export = models.DateField(blank=True, null=True)
	dateimportd = models.DateTimeField(db_column='dateImport', blank=True, null=True) 
	importd = models.DateField(db_column='import', blank=True, null=True)
	dateentercustoms = models.DateTimeField(db_column='dateEnterCustoms', blank=True, null=True) 
	entercustoms = models.DateField(db_column='enterCustoms', blank=True, null=True)
	dateimportleave = models.DateTimeField(db_column='dateImportLeave', blank=True, null=True) 
	importleave = models.DateField(db_column='importLeave', blank=True, null=True)
	objects = managers.TracksManager()
	withdates = managers.TracksWithDatesManager()

	class Meta:
		db_table = 'tracks'

class InternalTracks(BaseTracks):
	number = models.BigIntegerField()
	objects = managers.InternalTracksManager()
	withdates = managers.InternalTracksWithDatesManager()
	class Meta:
		db_table = 'internal_tracks'
