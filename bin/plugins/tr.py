# -*- coding: utf-8 -*-
# Turkish Post tracking plugin

from base import Base
from dateutil.parser import parse
import binascii
import os

class TrPost(Base):
	def __init__(self, track, instance):
		super(TrPost, self).__init__(track, instance)
		self.trackurl = 'http://gonderitakip.ptt.gov.tr/en/'
		self.captchaurl = 'http://gonderitakip.ptt.gov.tr/CaptchaSecurityImages.php?width=100&height=40&characters=5'
		self.captcha = '/tmp/trpost-{}.jpg'.format(binascii.b2a_hex(os.urandom(4)))
		self.need_dop = False
		self.datesDesc = {
			'dateAccept': ('Item Is Ready for Ship', 'Accepted'),
			'dateExport': (
				'Item was uplifted to flight to destination country',
				'The item was forwarded to the destination country'
			)
		}

	def execute(self):
		self.getUrl(self.trackurl)
		captcha_result = self.getUrl(self.captchaurl, fname=self.captcha)
		if not captcha_result or not self.captcha_is_image:
			if os.path.exists(self.captcha):
				os.remove(self.captcha)
			return False
		try:
			captcha_answer = self.lib.antigate_ocr(self.captcha, {'min_len': '5', 'max_len': '5'})
		except Exception as e:
			os.remove(self.captcha)
			raise e
		html = self.getUrl(
			self.trackurl, True, {
				'barkod': self.tracknum,
				'security_code': captcha_answer,
				'Submit': 'Tracking'
			}
		)
		if not html:
			return False
		ops = self.xpath('//div[@id="centertop"][3]/div/div/ul/li/table/tbody/tr/td[position()<6]/text()', html)
		if not ops:
			self.parse_err = 1
			return False
		ops = self.arChunk(ops, 4)
		ops.reverse()
		for name, desc in self.datesDesc.iteritems():
			for op in ops:
				if any(phrase in op[3] for phrase in desc):
					self.dates[name] = parse('{} {}'.format(op[1], op[2]), dayfirst=True)
					break
		return len(self.dates) > 0
