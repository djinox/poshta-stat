// gulp css/js minify tasks
const gulp = require('gulp');
const concat = require('gulp-concat');
const cleanCSS = require('gulp-clean-css');
const rename = require('gulp-rename');
const uglify = require('gulp-uglify');
const md5 = require('md5');
const imagehash = require('gulp-css-image-hash');
const redis = require('redis');
const md5File = require('md5-file');
const fs = require('fs');

const KEY_PREFIX = 'poshta-stat:1:rev:';
const JS_DEST = 'scripts.min.js';
const CSS_DEST = 'styles.min.css';
const DIST_DIR = './dist';

var setRevision = function(files, setFunc, endCb) {
	var processed = 0;
	for (var i = 0; i < files.length; i++) {
		(function(f) {
			fs.realpath(DIST_DIR + '/' + f, function(err, path) {
				if (err) throw err;
				md5File(path, function(err, hash) {
					if (!err) {
						setFunc(KEY_PREFIX + md5(f), "S'" + hash.substring(8, 16) + "'\np0\n."); // python pickle string repr
					}

					if (++processed === files.length) {
						endCb();
					}
				});
			});
		})(files[i]);
	}
};

gulp.task('minify-css', function() {
	return gulp.src(['poshta_stat/public/static/css/3rdparty/**/*.css', 'poshta_stat/public/static/css/style.css'])
		.pipe(concat('all.css'))
		.pipe(imagehash('./poshta_stat/public', ['jpg', 'png', 'ico']))
		.pipe(rename(CSS_DEST))
		.pipe(cleanCSS({compatibility: 'ie8'}))
		.pipe(gulp.dest(DIST_DIR));
});

gulp.task('minify-js', function() {
	var js_src = [
		'poshta_stat/public/static/js/3rdparty/**/*.js',
		'poshta_stat/public/static/js/ng/**/*.js'
	];
	return gulp.src(js_src)
		.pipe(concat('all.js'))
		.pipe(rename(JS_DEST))
		.pipe(uglify())
		.pipe(gulp.dest(DIST_DIR));
});

gulp.task('minify', ['minify-css', 'minify-js'], function(cb) {
	var redisClient = new redis.createClient({db: 1});

	redisClient.on('error', function(err) {
		console.error(err);
		cb();
	});

	var setMethod = function(key, val) {
		redisClient.set(key, val);
	};

	setRevision([JS_DEST, CSS_DEST], setMethod, function() {
		cb();
		redisClient.quit();
	});
});