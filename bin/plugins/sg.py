# -*- coding: utf-8 -*-
# SingPost tracking plugin

from base import Base
from dateutil.parser import parse
from xml.parsers.expat import ExpatError
from time import sleep
import subprocess
import xmltodict

class SgPost(Base):
	def __init__(self, track, instance):
		super(SgPost, self).__init__(track, instance)
		self.trackurl = 'http://www.singpost.com/index.php?option=com_sp_service&controller=tab_tracking&task=trackdetail&layout=show_detail&tmpl=component&ranumber=%s&type=%s'
		self.referer = None
		self.curl_path = '/opt/curl/bin/curl'
		self.need_dop = False
		self.timeout = 20
		self.datesDesc = {
			'dateAccept': ['received from customer', 'information received'],
			'dateExport': ['despatched to overseas'],
		}
		self.datesDescEms = {
			'dateAccept': ['Shipment ready', 'Notification of shipment confirmation', 'Pending shipment from Merchant/Shipper'],
			'dateExport': ['Handed over to Airline', 'Depart from Overseas Processing Facility'],
		}

	def __track_ems(self):
		self.referer = 'http://www.speedpost.com.sg/'
		pdata = {
			'btnTrack': 'Track',
			'controller': 'tracking',
			'field_item': '76',
			'limitTrackItem': '10',
			'option': 'com_content_left',
			'task': 'tracking',
			'txt_track_items_76': self.tracknum,
		}
		html = self.getUrl('http://www.speedpost.com.sg/index.php?quickTool=TrackItem', True, pdata)
		if not html:
			return False
		if 'there is no result' in html or 'there are too many request' in html:
			return False
		self.trackurl = 'http://www.speedpost.com.sg/index.php?controller=tracking&option=com_content_left&task=trackdetail&track_item_76={0}'
		html = self.getUrl(self.trackurl.format(self.tracknum))
		if not html:
			return False
		am = self.xpath('//table[@class="tbShippingPop tbGeneral"]/tr/td[position()<4]/text()', html)
		if not am:
			return False
		am = map(lambda s: s.strip(), am)
		ops = self.arChunk(am, 3)
		for name, desc in self.datesDescEms.iteritems():
			for oper in ops:
				if oper[2] in desc:
					self.dates[name] = parse(oper[0], dayfirst=True)
					break
		return len(self.dates) > 0

	def __track_post(self):
		self.getUrl('http://www.singpost.com/?betareturn=1')
		self.referer = 'http://www.singpost.com/'
		pdata = {
			'track_items': self.tracknum,
			'flag_cpt': '0',
			'option': 'com_sp_service',
			'controller': 'tab_tracking',
			'task': 'tracking',
			'Itemid': '377',
			'limitTrackItem': '20',
		}
		html = self.getUrl('http://www.singpost.com/track-items', True, pdata)
		ptype = '1'
		if not html:
			return False
		if 'Item No/Ref No' not in html:
			pdata['isThreeMonthOldTrackItem'] = '1'
			pdata['itemOldTrackNumber[0]'] = self.tracknum
			sleep(3)
			html = self.getUrl(self.referer, True, pdata)
			if not html or 'Item No/Ref No' not in html:
				return False
			ptype = '2'
		html = self.getUrl(self.trackurl % (self.tracknum, ptype))
		if not html:
			return False
		am = self.xpath('//table[@class="commonTable"]/tbody/tr[position()>1]/td/text()', html)
		if not am:
			self.parse_err = 1
			return False
		am = map(lambda s: s.strip(), am)
		ops = self.arChunk(am, 2)
		for dtName, desc in self.datesDesc.iteritems():
			found = False
			for operation in ops:
				for pattern in desc:
					if pattern in operation[1].lower():
						self.dates[dtName] = parse(operation[0], dayfirst=True)
						found = True
						break
				if found:
					break
		bag = self.xpath('//span[@id="lblDespatchNo"]/*/text()', html)
		if bag:
			self.dates['bagNo'] = bag[0]
		return len(self.dates) > 0

	def __track_post_api(self):
		self.trackurl = 'https://prdesb1.singpost.com/ma/GetItemTrackingDetails'
		pattern = '<ItemTrackingDetailsRequest xmlns="http://singpost.com/paw/ns"><SystemID>ABC</SystemID><ItemTrackingNumbers><TrackingNumber>{}</TrackingNumber></ItemTrackingNumbers></ItemTrackingDetailsRequest>'
		try:
			xml = subprocess.check_output([self.curl_path, '-s', '-H', 'Content-Type: application/xml', '-d', pattern.format(self.tracknum), self.trackurl])
		except subprocess.CalledProcessError:
			return False
		try:
			parsed = xmltodict.parse(xml)
		except ExpatError:
			self.parse_err = 2
			return False
		try:
			detail = parsed['ItemTrackingDetailsResponse']['ItemsTrackingDetailList']['ItemTrackingDetail']
		except (KeyError, TypeError):
			self.parse_err = 3
			return False
		if 'TrackingNumberFound' not in detail or detail['TrackingNumberFound'] == 'false':
			return False
		try:
			ops = detail['DeliveryStatusDetails']['DeliveryStatusDetail']
			if not ops:
				return False
		except KeyError:
			self.parse_err = 4
			return False
		if not isinstance(ops, list):
			ops = [ops]
		for datename, desc in self.datesDesc.iteritems():
			for oper in ops:
				if any(p in oper['StatusDescription'].lower() for p in desc):
					self.dates[datename] = parse(oper['Date']).replace(tzinfo=None, microsecond=0)
					break
		if detail['DispatchNo']:
			self.dates['bagNo'] = detail['DispatchNo']
		return len(self.dates) > 0

	def execute(self):
		return self.__track_post_api() if self.tracknum[0] == 'R' else self.__track_ems()
