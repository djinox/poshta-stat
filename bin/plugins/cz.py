#-*- coding: utf-8 -*-
# Czech post tracking plugin

from base import Base
from dateutil.parser import parse
import json
import re

class CzPost(Base):
	def __init__(self, track, instance):
		super(CzPost, self).__init__(track, instance)
		self.trackurl = 'https://www.postaonline.cz/trackandtrace/-/zasilka/cislo?parcelNumbers=%s'
		self.need_dop = False
		self.timeout = 20
		self.datesDesc = {
			'dateAccept': u'Podání zásilky',
			'dateExport': u'Odeslání zásilky do země určení',
		}
		
	def execute(self):
		html = self.getUrl(self.trackurl % self.tracknum)
		if not html:
			return False
		am = self.xpath('//div[@class="item-detail-content"]/table[@class="datatable2"]/tr/td[position()=1 or position()=2]/text()', html)
		if not am:
			return False
		ops = self.arChunk(am, 2)
		ops.reverse()
		for name, desc in self.datesDesc.iteritems():
			for oper in ops:
				if desc in oper[1]:
					self.dates[name] = parse(oper[0], dayfirst=True)
		return len(self.dates) > 0