angular.module('psApp').controller(
	'summaryCtrl',
	['$scope', '$http', '$location', '$filter', 'QSService', '$window',
	function summaryCtrl($scope, $http, $location, $filter, QSService, $window) {
		var entryPoint = '/api/get_stats_detailed';
		var defaultFilterParams = {date: $filter('date')(new Date, 'dd.MM.yyyy')};

		$scope.searchIsActive = false;
		$scope.showError = false;
		$scope.countries = $window.PS.cmap;
		$scope.dateTableTitles = {
			accept: 'Поступило на почту отправителя (прием)',
			export: 'Отправка из страны отправителя (экспорт)',
			importd: 'Поступление в ММПО Украины (импорт)',
			entercustoms: 'Передача на таможенное оформление',
			importleave: 'Отправка из места международного обмена',
			arrive: 'Поступление в почтовое отделение вручения',
			deliver: 'Вручено адресату',
			create: 'Добавлено новых'
		};
		$scope.dateDisplayOrder = ['create', 'accept', 'export', 'importd', 'entercustoms', 'importleave', 'arrive', 'deliver'];

		$scope.orderedTableTitles = [];
		for (var i=0; i<$scope.dateDisplayOrder.length; i++) {
			$scope.orderedTableTitles.push({
				key: $scope.dateDisplayOrder[i],
				value: $scope.dateTableTitles[$scope.dateDisplayOrder[i]]
			});
		}

		$scope.validate = function() {
			var match = /^(\d{2})\.(\d{2})\.(\d{4})$/.exec($scope.filterParams.date);
			if (match) {
				var date = new Date(match[3], parseInt(match[2])-1, match[1]);
				return date.getDate() === parseInt(match[1])
					&& date.getMonth()+1 === parseInt(match[2])
					&& date.getFullYear() === parseInt(match[3]);
			}
			return false;
		};

		$scope.loadSummary = function() {
			$scope.searchIsActive = true;
			$scope.showError = false;

			$http.post(entryPoint, $scope.filterParams).success(function(data) {
				data.status ? $scope.data = data : $scope.showError = true;
			}).error(function() {
				$scope.showError = true;
			}).finally(function() {
				$scope.searchIsActive = false;
			});
		};

		$scope.filterParams = QSService.getFilters(defaultFilterParams);
		if (!$scope.validate()) {
			$scope.filterParams = {date: $filter('date')(new Date, 'dd.MM.yyyy')};
			$location.search({});
		}

		$scope.setDate = function(e) {
			var dt = e.format();
			if (dt != $scope.filterParams.date) {
				$scope.filterParams.date = dt;
				$location.search($scope.filterParams);
			}
		};

		$scope.$watch(function() { return $location.search(); }, function() {
			QSService.getFilters($scope.filterParams);
			$scope.loadSummary();
		}, true);
	}]
);
