# -*- coding: utf-8 -*-
# Deutsche Post tracking plugin (Post & DHL)

from base import Base
from dateutil.parser import parse
import re

class DePost(Base):
	def __init__(self, track, instance):
		super(DePost, self).__init__(track, instance)
		self.trackurl = 'https://www.deutschepost.de/sendung/simpleQueryResult.html'
		self.need_dop = True
		self.datesDesc = {
			'dateAccept': u'eingeliefert',
			'dateExport': u'internationalen logistikzentrum frankfurt zur weiterbeförderung',
		}

	def execute(self):
		if self.tracknum[0] == 'R':
			day, month, year = self.customData.split('.')
			post_data = {
				"form.sendungsnummer": self.tracknum,
				"form.einlieferungsdatum_monat": month,
				"form.einlieferungsdatum_tag": day,
				"form.einlieferungsdatum_jahr": year,
			}
			html = self.getUrl(self.trackurl, True, post_data)
			if not html:
				return False
			stat = self.xpath('//div[@class="dp-table native-swipeable"]//td[@class="grey"]/text()', html)
			if not stat:
				self.parse_err = 1
				return False
			stat = stat[0].strip().lower()
			if 'unter dem von ihnen' in stat or 'es konnten keine informationen' in stat:
				return False
			m = re.search(r'am (\d{2}\.\d{2}\.\d{4})', stat)
			if not m:
				self.parse_err = 2
				return False
			for name, pattern in self.datesDesc.iteritems():
				if pattern in stat:
					self.dates[name] = parse(m.group(1))
			return len(self.dates) > 0
		elif self.tracknum[0] == 'C':
			return False
		return False