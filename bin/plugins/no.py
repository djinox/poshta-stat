# -*- coding: utf-8 -*-
# Posten.no (Norwegia)

from base import Base
from dateutil.parser import parse

class NoPost(Base):
	def __init__(self, track, instance):
		super(NoPost, self).__init__(track, instance)
		self.trackurl = 'http://sporing.posten.no/sporing.html?lang=en&q=%s'
		self.need_dop = False
		self.datesDesc = {
			'dateAccept': ['Package has been handed in to post office'],
			'dateExport': ['Package has been sent from origin country'],
		}

	def execute(self):
		html = self.getUrl(self.trackurl % self.tracknum)
		if not html:
			return False
		table = self.xpath('//table[@class="sporing-sendingandkolli-eventlist"]/tbody/tr/td/text() | //table[@class="sporing-sendingandkolli-eventlist"]/tbody/tr/td/div/text()', html)
		if not table:
			self.parse_err = 1
			return False
		table = map(lambda s: s.strip(), table)
		for i, e in enumerate(table):
			if len(e) < 1:
				del table[i]
		ops = self.arChunk(table, 3)
		ops.reverse()
		for name,desc in self.datesDesc.iteritems():
			for oper in ops:
				if oper[0] in desc:
					self.dates[name] = parse(oper[1])
		return len(self.dates) > 0

