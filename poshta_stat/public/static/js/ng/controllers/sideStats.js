angular.module('psApp').controller(
	'sideStatsCtrl',
	['$scope', '$http', '$localStorage', 'DataShare',
	function sideStatsCtrl($scope, $http, $localStorage, DataShare) {
		var entryPoint = '/api/get_stats';
		var lifeTime = 900;

		$scope.$watch(function() { return DataShare.service; }, function() {
			$scope.service = DataShare.service;
			$scope.loadSideStats();
		}, true);

		$scope.data = {};
		$scope.errmsg = 'н/д';
		$scope.utime = function() {
			return parseInt(new Date().getTime() / 1000);
		};

		$scope.getLocalStorageKey = function() {
			return 'sideStats_' + $scope.service;
		};

		$scope.loadSideStatsFromLocalStorage = function() {
			var data = $localStorage[$scope.getLocalStorageKey()];
			if (data) {
				if ($scope.utime() - data.expire <= lifeTime) {
					$scope.data = data.json;
					return true;
				}
			}

			return false;
		};

		$scope.loadSideStats = function() {
			$scope.summaryImgClass = '';
			$scope.summaryImgFlag = '/static/images/blank.gif';

			if (!$scope.loadSideStatsFromLocalStorage()) {
				$http.post(entryPoint, {service: $scope.service}).success(function(data) {
					if (data.status) {
						$scope.data = data;
						$localStorage[$scope.getLocalStorageKey()] = {json: data, expire: $scope.utime() + lifeTime};
					}
				});
			}
		};

		$scope.$watch(function(){ return $scope.data; }, function() {
			if ($scope.data.image) {
				if ($scope.data.image.length == 2) {
					$scope.summaryImgClass = 'flag-med ' + $scope.data.image;
				} else {
					$scope.summaryImgFlag = '/static/images/' + $scope.data.image + '.png';
				}
			}
		}, true);

		// just for controller ng-init on track details page and other pages with this ctrl
		$scope.setService = function(service) {
			DataShare.service = service;
		};
	}]
);