# -*- coding: utf-8 -*-
# Ireland Post tracking plugin

from base import Base
from dateutil.parser import parse

class IePost(Base):
	def __init__(self, track, instance):
		super(IePost, self).__init__(track, instance)
		self.trackurl = 'https://track.anpost.ie/TrackStatus.aspx?item=%s&sender='
		self.need_dop = False
		self.datesDesc = {
			'dateAccept': ['ITEM RECEIVED', 'ON ROUTE TO CUSTOMER'],
			'dateExport': ['ITEM FORWARDED TO', 'ITEM DESPATCHED FROM'],
		}

	def execute(self):
		html = self.getUrl(self.trackurl % self.tracknum)
		if not html:
			return False
		tbl = self.xpath('//tr[@class="statusAlternate" or @class="statusRow"]/td[position()!=2]/text()', html)
		if not tbl:
			self.parse_err = 1
			return False
		tbl = self.arChunk(tbl, 3)
		tbl.reverse()
		for name, desc in self.datesDesc.iteritems():
			for ref in desc:
				found = False
				for status in tbl:
					if ref in status[0]:
						self.dates[name] = parse(' '.join([status[1], status[2]]))
						found = True
						break
				if found:
					break
		return len(self.dates) > 0
