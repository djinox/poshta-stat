# -*- coding: utf-8 -*-
# HongKong Post tracking plugins

from base import Base
from dateutil.parser import parse
import re

class HkPost(Base):
	def __init__(self, track, instance):
		super(HkPost, self).__init__(track, instance)
		self.trackurl = 'http://app3.hongkongpost.hk/CGI/mt/mt4result.jsp'
		self.need_dop = False
		self.datesDesc = {
			'dateAccept': ['was posted', 'Item posted'],
			'dateExport': ['left Hong Kong'],
		}

	def execute(self):
		self.getUrl('http://app3.hongkongpost.hk/CGI/mt/getSubmit.jsp', True, {})
		html = self.getUrl(self.trackurl, True, {'tracknbr': self.tracknum, 'submit': 'Enter'})
		if not html:
			return False
		if self.tracknum[0] == 'R':
			if self.xpath('//span[@class="textNormalBlack"]', html):
				status = self.xpath('//div[@id="clfContent"]/text()[7]', html)
				if not status:
					self.parse_err = 1
					return False
				m = re.search(r'item \(%s\) (.+) (\d{1,2}-\w{3}-\d{4})' % self.tracknum, status[0].strip())
				if not m:
					self.parse_err = 2
					return False
				for name, desc in self.datesDesc.iteritems():
					for pattern in desc:
						if pattern in m.group(1):
							self.dates[name] = parse(m.group(2))
							break
				return len(self.dates) > 0
		elif self.tracknum[0] in ('E', 'C'):
			mail_types = {'E': 'ems_out', 'C': 'parcel_ouw'}
			self.trackurl = 'http://app3.hongkongpost.hk/CGI/mt/e_detail4.jsp?mail_type={0}&tracknbr={1}&localno={1}'
			html = self.getUrl(self.trackurl.format(mail_types[self.tracknum[0]], self.tracknum))
			if not html:
				return False
			ops = self.xpath('//table[@class="detail"][position()=2]/tr/td/text()', html)
			if not ops:
				return False
			ops = self.arChunk(ops, 3)
			for name, desc in self.datesDesc.iteritems():
				found = False
				for status in ops:
					for pattern in desc:
						if pattern in status[2]:
							self.dates[name] = parse(status[0])
							found = True
							break
					if found:
						break
			return len(self.dates) > 0
		return False